"""
About dialog
"""

"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt4.QtCore import *
from PyQt4.QtGui import *

import info

class Ui_AboutDialog(QDialog):
    """About dialog.

    Most of the information is taken from the info module.

    """
    def __init__(self, parent=None):
        super(Ui_AboutDialog, self).__init__(parent)
        self.setWindowTitle("About %s" % info.name)
        self._layout = QVBoxLayout()

        # this part controls what to be printed
        self._addLabel('', info.description)
        self._addLabel('Version:', info.version)
        if info.long_version:
            self._addLabel('Source:', info.long_version)
        self._addLabel('', info.author)
        self._addLabel('', info.author_email)
        self._addLabel('', info.url)
        self._addLabel('License:', info.license)

        button = QDialogButtonBox(QDialogButtonBox.Ok)
        button.setCenterButtons(True)
        button.accepted.connect(self.accept)
        self._layout.addWidget(button)
        self._layout.setSizeConstraint(QLayout.SetFixedSize)
        self.setLayout(self._layout)

    def _addLabel(self,key,value=''):
        label = QLabel(key + ' ' + value)
        label.setAlignment(Qt.AlignCenter)
        label.setWordWrap(True)

        self._layout.addWidget(label)

if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)

    win = Ui_AboutDialog()
    win.show()

    app.exec_()
