"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

from pint import UnitRegistry
from pint.errors import DimensionalityError
import logging

Unit = UnitRegistry(autoconvert_offset_to_baseunit = True)
Unit.define('year = 31557600.0000 * second')
Unit.define('darcy = 1.0e-12 * m ** 2')
Unit.define('t2sec = second; offset: 0.0000')
Unit.define('mRL = meter')
Unit.define('D = darcy')

def setTimeOffset(offset):
    if isinstance(offset, type(Unit(''))):
        offset_sec = offset.to('second').magnitude
    elif isinstance(offset, str):
        offset_sec = Unit(offset).to('second').magnitude
    else:
        offset_sec = offset
    Unit.define('t2sec = second; offset: %f' % offset_sec)
# setTimeOffset('1953.0 years')

def timeOffset(unit="second"):
    """Given a unit string, returns the current timeOffset as a float in terms of that unit"
    """
    offset = Unit.Quantity(0,'t2sec').to(unit).magnitude
    #eg set time offset to 1 day, then returns 0.9999999 days - i think just a floating point issue? but maybe a better way of doing it idk?
    ##probably need some error checking here?
    return offset


def _initDefaults():
    units = [
        ('year', 'Time', ['t2sec', 'second', 'day', 'year']),
        ('bar', 'Pressure', ['Pa', 'bar', 'kPa', 'MPa', 'psi']),
        ('degC', 'Temperature', ['degC', 'degF', 'kelvin']),
        ('m', 'Length', ['mm', 'm', 'km', 'feet']),
        ('mD', 'Area', ['darcy', 'D', 'mD', 'm**2']),
        ('kJ/kg', 'Enthalpy', ['J/kg', 'kJ/kg']),
        ('kg', 'Mass', ['kg', 'g', 'tonne']), # tonne is 'metric_ton'
        ('kg/s', 'Mass rate', ['kg/s', 'tonne/day', 'tonne/hr', 'kilotonne/day', 'megatonne/day']),
        ('kg/m**3', 'Density', ['kg/m**3']),
        ('', '', []), # for dimensionless quantities like saturation etc.
    ]
    default = {}
    for u, name, possible in units:
        unit = Unit(u)
        dim = frozenset(dict(unit.dimensionality).items())
        default[dim] = u, name, possible
    return default
_defaults = _initDefaults()

def defaultUnit(unit):
    """ Given a unit string, returns the application's default unit.
    """
    u = Unit(unit)
    dim = getDim(unit)
    try:
        return _defaults[dim][0]
    except KeyError:
        logging.warning('Unit %s not found in default list, use base SI units as default' % unit)
        sibu = str(u.to_base_units().units).replace('gram', 'kilogram')
        _defaults[frozenset(dict(Unit(sibu).dimensionality).items())] = (sibu, sibu, [])
        return sibu

def setDefaultUnit(new_unit, name=''):
    """ change default unit _defaults

    Note unit should be like 'kg', instead of weight etc.  name is used only
    when a new default unit is created.
    """
    u = Unit(new_unit)
    dim = getDim(new_unit)
    try:
        _defaults[dim] = (new_unit, ) + _defaults[dim][1:]
    except KeyError:
        logging.warning('Unit %s not found in default list' % new_unit)
        if not name:
            name = str(dim)
        _defaults[dim] = (new_unit, name, [str(k) for k in u.compatible_units()])
        logging.warning('added ' + str(_defaults[dim]))

def unitChoices(unit):
    """ Given a unit string, returns the choices of units with same physical
    quantity.
    """
    try:
        dim = frozenset(dict(Unit(str(unit)).dimensionality).items())
        return _defaults[dim][2]
    except KeyError:
        return []

def guessUnits(variable, lst=None):
    """ Use this to find units from name supplied by AUTOUGH2 outputs.
    """
    var = {
        # AUT2 element
        "Pressure": 'pascal',
        "Capillary pressure": 'pascal',
        "Air partial pressure": 'pascal',
        "Temperature": 'degC',
        "Enthalpy": 'J/kg',
        # AUT2 generation
        "Generation rate": 'kg/s', # TODO can be kg/s or J/s
        "Wellbore pressure": 'Pa',
        "Gas density": 'kg/m**3',
        "Liquid density": 'kg/m**3',
        # AUT2 connection
        "Mass flow": 'kg/s',
        "Heat flow": 'J/s',
        "Gas mass flow": 'kg/s',
        "Liquid mass flow": 'kg/s',
        "Gas velocity": 'm/s',
        "Liquid velocity": 'm/s',
        # T2 element
        "P": 'Pa',
        "T": 'degC',
        "PCAP": 'Pa',
        "DG": 'kg/m**3',
        "DL": 'kg/m**3',
        # T2 connection
        "FLOH": 'J/s',
        "FLOH/FLOF": 'J/kg',
        "FLOF": 'kg/s',
        "FLO(GAS)": 'kg/s',
        "VAPDIF": 'kg/s',
        "FLO(LIQ.)": 'kg/s',
        "VEL(GAS)": 'm/s',
        "VEL(LIQ.)": 'm/s',
        # T2 generation
        "GENERATION RATE": 'kg/s', # TODO can be kg/s or J/s
        "ENTHALPY": 'J/kg',
        "P(WB)": 'Pa',
        # Waiwera cell field
        "fluid_pressure": 'Pa',
        "fluid_temperature": 'degC',
        "fluid_liquid_capillary_pressure": 'Pa',
        "fluid_liquid_density": 'kg/m***3',
        "fluid_liquid_internal_energy": 'J',
        "fluid_liquid_specific_enthalpy": 'J/kg',
        "fluid_liquid_viscosity": 'kg/m/s',
        "fluid_vapour_capillary_pressure": 'Pa',
        "fluid_vapour_density": 'kg/m**3',
        "fluid_vapour_internal_energy": 'J',
        "fluid_vapour_specific_enthalpy": 'J/kg',
        "fluid_vapour_viscosity": 'kg/m/s',
        "fluid_water_partial_pressure": 'Pa',
        # Waiwera source field
        "source_enthalpy": 'J/kg',
        "source_rate": 'kg/s', # TODO can be kg/s or J/s
    }
    if variable in var:
        return var[variable]
    else:
        if len(variable) > 1:
            for v in var.keys():
                if v.startswith(variable):
                    # if partial match eg. AUTOUGH2 column names
                    return var[v]
        return ''

def getDim(unit):
    """ Returns the dimentionality of a unit string (eg. kg)

    This is useful as the _defaults is keyed by dimensionality of units, instead
    of common names like "Weight" etc.  Dimensionality of units are unique but
    common names are not.
    """
    if isinstance(unit, str) or isinstance(unit, unicode):
        u = Unit(unit)
    else:
        # hopefully pint.unit.Quantity or pint.unit.Unit
        u = unit
    dim = frozenset(dict(u.dimensionality).items())
    return dim

def niceName(unit):
    """ Given a unit string, returns the physical quantity name.
    """
    dim = frozenset(dict(Unit(unit).dimensionality).items())
    return _defaults[dim][1]

def shortUnit(unit):
    """ Given a unit string, returns a abbreviated name.
    """
    return ''.join('{:~}'.format(Unit(unit)).split(' ')[1:])

def is_comp(a,b):
    """ check if two units (strings) are compatible """
    comp = True
    try:
        Unit(a).to(b)
    except DimensionalityError:
        comp = False
    return comp

def connectionFluxAllowed(v):
    """ Check if variable (listing.connection.column_name) is allowed to be used
    in 'flux thru top of block'.  Probably only heat flow and mass flow make
    sense. """
    u = guessUnits(v)
    okays = ['J/s', 'kg/s']
    return any([is_comp(u,c) for c in okays])

def find_rate_enth_names(col_names):
    """ Given listingtable.column_name, this returns tuple of column names
    matching 'generation rate' and 'enthalpy'.  This covers the known cases of
    TOUGH2's capitals and AUTOUGH2's truncated headings.  If not matched, None
    will be returned. """
    col_names_lower = [h.lower() for h in col_names]
    r, e = None, None
    for n, nlower in zip(col_names, col_names_lower):
        if 'generation rate'.startswith(nlower):
            r = n
        if 'enthalpy'.startswith(nlower):
            e = n
    if r is None or e is None:
        return None
    else:
        return (r,e)



def _test_conversion_performence():
    import timeit
    n = 5000
    setup = '\n'.join([
        "from pint import UnitRegistry",
        "Unit = UnitRegistry()",
        "import numpy as np",
        "a = np.arange(1000.) * Unit('Pa')",
        ])
    test = '\n'.join([
        "a.ito('bar')",
        "a.ito('Pa')",
        ])
    print timeit.timeit(stmt=test, setup=setup, number=n)
    test = '\n'.join([
        "a.ito('Pa')",
        "a.ito('Pa')",
        ])
    print timeit.timeit(stmt=test, setup=setup, number=n)
    test = '\n'.join([
        "a.ito('pascal')",
        "a.ito('Pa')",
        ])
    print timeit.timeit(stmt=test, setup=setup, number=n)

def _test_attach_vs_separate():
    import timeit
    n = 500
    setup = '\n'.join([
        "from pint import UnitRegistry",
        "Unit = UnitRegistry()",
        "import numpy as np",
        "a = np.arange(10000.) * Unit('Pa')",
        "b = np.arange(10000.)",
        ])
    test = '\n'.join([
        "a.ito('bar')",
        ])
    print timeit.timeit(stmt=test, setup=setup, number=n)
    test = '\n'.join([
        "b = (b*Unit('Pa')).to('bar').magnitude",
        ])
    print timeit.timeit(stmt=test, setup=setup, number=n)
    test = '\n'.join([
        "b = b * Unit('Pa')",
        "b.ito('bar')",
        "b = b.magnitude",
        ])
    print timeit.timeit(stmt=test, setup=setup, number=n)

if __name__ == '__main__':
    # _test_attach_vs_separate()
    # _test_conversion_performence()

    import unittest

    class TestUnitUtilities(unittest.TestCase):
        def test_compatible(self):
            vs = [
                # AUT2 connection
                ("Mass flow", True),
                ("Heat flow", True),
                ("Enthalpy", False),
                ("Gas mass flow", True),
                ("Liquid mass flow", True),
                ("Gas velocity", False),
                ("Liquid velocity", False),
                # T2 connection True
                ("FLOH", True),
                ("FLOH/FLOF", False),
                ("FLOF", True),
                ("FLO(GAS)", True),
                ("VAPDIF", True),
                ("FLO(LIQ.)", True),
                ("VEL(GAS)", False),
                ("VEL(LIQ.)", False),
                ]
            for v,ok in vs:
                self.assertEqual(connectionFluxAllowed(v), ok, v)

        def test_get_dim(self):
            ts = [
                ('year', 'second', True),
                ('year', 'year', True),
                ('year', 'kg', False),
                ('year', 'kg', False),
                ('kg/s', 'tonne/day', True),
                (Unit('kg/s'), 'tonne/day', True), # can pass in quantity
                (Unit('kg/s').u, 'tonne/day', True), # can pass in unit
                (Unit('kg/s').u, 'tonne', False),
            ]
            for a, b, same in ts:
                self.assertEqual(getDim(a) == getDim(b), same)
            ts2 = [
                (frozenset([(u'[length]', 1.0)]), 'm'),
                (frozenset([(u'[mass]', 1.0)]), 'kg'),
                (frozenset([(u'[length]', 2.0), (u'[time]', -2.0)]), 'kJ/kg'),
            ]
            for a, b in ts2:
                self.assertEqual(a, getDim(b))

        def test_set_default_unit(self):
            ### original default of pressu is bar
            punit = 'bar'
            # tests _defaults structure, defaultUnit(), getDim()
            self.assertEqual(defaultUnit('Pa'), _defaults[getDim('bar')][0])
            # change tp Pa
            setDefaultUnit('Pa')
            self.assertEqual('Pa', defaultUnit(punit))
            # change tp psi
            setDefaultUnit('psi')
            self.assertEqual('psi', defaultUnit(punit))
            ###
            gunit = 'm/s/s'
            setDefaultUnit('m/s/s')
            self.assertEqual('m/s/s', defaultUnit(gunit))

    class TestDataValidityTests(unittest.TestCase):
        def test_find_rate_enth(self):
            col_names = ["Generation rate", "Enthalpy", "Wellbore pressure", "Gas density", "Liquid density"]
            self.assertEqual(find_rate_enth_names(col_names),
                ("Generation rate", "Enthalpy"))

            col_names = ["Generation rat", "Enthalp", "Wellbore pressure", "Gas density", "Liquid density"]
            self.assertEqual(find_rate_enth_names(col_names),
                ("Generation rat", "Enthalp"))

            col_names = ["GENERATION RATE", "ENTHALPY", "P(WB)"]
            self.assertEqual(find_rate_enth_names(col_names),
                ("GENERATION RATE", "ENTHALPY"))

            col_names = ["GENERATION RATE", "P(WB)"]
            self.assertEqual(find_rate_enth_names(col_names),
                None)

            col_names = ["ENTHALPY", "P(WB)"]
            self.assertEqual(find_rate_enth_names(col_names),
                None)

            col_names = ["P(WB)"]
            self.assertEqual(find_rate_enth_names(col_names),
                None)

    unittest.main(verbosity=2)

