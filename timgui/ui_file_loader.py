"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt4.QtCore import *
from PyQt4.QtGui import *

import StringIO
import time
import logging
from contextlib import contextmanager

@contextmanager
def stdout_redirector(stream):
    import sys
    old_stdout = sys.stdout
    sys.stdout = stream
    try:
        yield
    finally:
        sys.stdout = old_stdout

def load_file_progress(filename, loading_func,
                       obj_ok=None, progress_qobj=None,
                       name='file', code_name='code: '):
    """ This is a fancy *loader* that runs a typical user-supplied function with
    all the fancy exception handling with logging.  It also supports progress
    bar and critical message box when something goes wrong.

    Returns the object returned by the user func if things are ok.  Otherwise
    None will be returned.  An optional checker function can also be supplied,
    which should return True if the loaded object is good.

    This is especially useful when the loading routine does not raise Exception
    when things are not quite right.

    Example:
        import log # needed for the logging mechanism
        # (loading a mulgrid object using PyTOUGH)
        geo = load_file_progress(filename,        # user func accepts this
                                 mulgrid,         # user-supplied func
                                 obj_ok=geo_ok,   # user-supplied func
                                 progress_qobj=geo_progress,
                                 name='Mulgrid Geometry File',
                                 code_name='PyTOUGH')

    NOTE: PyTOUGH does not necessary raise exceptions when something goes
    wrong.  It can return an empty grid while print something on STDOUT.
    Context stdout_redirector is used here to capture these 'error
    messages'.  __repr__ of mulgrids also used to display more information.

    For progress bar, progress_qobj should be a simple QObject, user func use
    the object to emit signals of progress, eg.:
        # for QProgressBar(), so convert to int, assume 0-100
        progress_qobj.emit(SIGNAL("progress"), 70)
        # or text
        progress_qobj.emit(SIGNAL("progress_text"), 'loading abc')
    """
    if isinstance(progress_qobj, QObject):
        # progress bar, who should be the dialog parent??
        progress = QProgressDialog("Loading %s..." % name, QString(), 0, 100)
        progress.setAutoClose(True)
        progress.show()
        QObject.connect(progress_qobj, SIGNAL('progress'), progress.setValue)
        QObject.connect(progress_qobj, SIGNAL('progress_text'), progress. setLabelText)
        progress.setValue(0)
    msg1, msg2, msg3 = '', '', ''
    try:
        f = StringIO.StringIO()
        with stdout_redirector(f):
            start_time = time.time()
            obj = loading_func(filename)
            msg = 'Finished loading %s after %.1f sec' % (name, (time.time()-start_time))
        m = f.getvalue()
        if m: msg3 = code_name + ': ' + f.getvalue()
        f.close()
        if callable(obj_ok):
            # check if object is ok
            if not obj_ok(obj):
                raise Exception(msg3)
    except Exception as e:
        msg1 = 'Failed to open %s: %s' % (name, filename)
        msg2 = str(e)
        obj = None
    # if failed to have a good object
    if obj is not None:
        logging.info(msg)
        if msg3: logging.info(msg3)
    else:
        logging.error(msg1)
        logging.error(msg2)
        QMessageBox.critical(None,
                             "Error loading %s" % name,
                             "\n\n".join([msg1, msg2]))
    if isinstance(progress_qobj, QObject):
        # finish progress bar
        progress.setValue(99)
        del progress
    return obj


class Ui_LoadFile(QWidget):
    """ A widget that let user browse and select a file.  Hints and status will
    be shown.  A function can be used to open and check the file, return status
    of good or bad.

    - title, prompt: strings used in the UI
    - init: string message for second label before the file object is loaded
    - loaded: if a simple string, used in second label after file is loaded,
              if a user specified function, it will be called (with filename),
              it should return (msg, obj), the msg will be displayed, obj will
              used as .obj property.  Useful as a file checker that spits proper
              message for user.

    Note a signal 'file_selected' will be emitted after user selected a file.
    If user supplies callable loaded, .obj property may be the loaded object.
    """
    def __init__(self, parent=None,
                 title='Load a file',
                 prompt='Select a file:',
                 init='',
                 loaded='loaded',
                 ):
        super(Ui_LoadFile, self).__init__(parent)
        self.init_text = init
        self.setWindowTitle(title)
        self.prompt_label = QLabel(prompt)
        self.browse_button = QPushButton('...')
        self.preview_label = QLabel(self.init_text)
        layout = QGridLayout()
        layout.addWidget(self.prompt_label, 0, 0, 1, 1)
        layout.addWidget(self.browse_button, 0, 1, 1, 1)
        layout.addWidget(self.preview_label, 1, 0, 1, 2)
        self.setLayout(layout)

        self.filename = None
        self.obj = None
        self.loaded = loaded

        QObject.connect(self.browse_button, SIGNAL('clicked()'), self.loadFile)

    def loadFile(self):
        fname = QFileDialog.getOpenFileName(self,
            self.prompt_label.text())
        if not fname.isEmpty():
            self.filename = str(fname)
            if callable(self.loaded):
                msg, obj = self.loaded(self.filename)
                self.preview_label.setText(msg)
                self.obj = obj
            else:
                self.preview_label.setText(str(self.loaded))
            self.emit(SIGNAL("file_selected"), self.filename)
        else:
            self.preview_label.setText(self.init_text)
            self.filename = None
            self.obj = None



if __name__ == '__main__':
    import sys
    from mulgrids import *
    import log

    app = QApplication(sys.argv)

    def load(filename):
        from t2model import mulgrid, geo_progress
        def geo_ok(geo):
            if geo.num_blocks > 0:
                return True
            else:
                return False
        geo = load_file_progress(filename,
                                 mulgrid,
                                 obj_ok=geo_ok,
                                 progress_qobj=geo_progress,
                                 name='Mulgrid Geometry File',
                                 code_name='PyTOUGH')
        msg = '%s:\n%s' % (filename, str(geo))
        return msg, geo

    win = Ui_LoadFile(title='XYZ',
                      prompt='Open Geometry file',
                      loaded=load
                      )

    def test_signal(a):
        print a
    QObject.connect(win, SIGNAL('file_selected'), test_signal)

    win.show()
    app.exec_()
