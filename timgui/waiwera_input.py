"""For general management of waiwera data files. No gui elements"""
import logging


#default values for waiwera rock type parameters
#default for dry_conductivity is whatever wet_conductivity is, so must be managed outside dictionary when used in code
WAIWERA_ROCK_DFALT = {
    "porosity": 0.1,
    "permeability": 10e-13,
    "name": "",
    "wet_conductivity": 2.5,
    "specific_heat": 1000,
    "density":2200,

}

def rock_cells(rock,dat):
    """returns a list of indices of all cells (specified via zone or cell) with that rock type """
    #rock is a dictionary (json) object describing parameters for one rock type
    #dat is a dictionary loaded from a waiwera input json file
    cells = set()
    zones = []
    unsupportedZones = []
    if 'cells' in rock:
        cells.update(rock['cells'])

    if 'zones' in rock:
        if isinstance(rock['zones'],unicode):
            zoneName = rock['zones']
            zones = [ dat['mesh']['zones'][zoneName] ]
        else:
            zones = [ dat['mesh']['zones'][zoneName]  for zoneName in rock['zones'] ]

        unsupportedZones = False

        for zone in zones:
            if isinstance(zone,list):
                cells.update(zone)
            elif 'cells' in zone:
                cells.update(zone['cells'])
            else:
                unsupportedZones = True

        if unsupportedZones:
            rockName = rock.get('name','Unnamed')
            #consider different way of handling this exception
            logging.warning('Unable to find all cells with %s rock type, as it has been applied to unsupported zone types' % rockName)

    return list(cells)

def update_cells_rock(dat, cell_idx, to_rocktype):
    """ Update cells' rock type assignment, cell_idx waiwera cell indices
    number.  to_rocktype is index of tha target rock type.

    NOTE! waiwera cell index does NOT include atmosphere blocks, index 0 is
    mulgrid's .num_atmosphere_blocks

    NOTE! if to_rocktype index is not valid (includes -1), it will result those
    cells being removed from all other rock types.  effectively making them
    default rock type in waiwera.
    """
    for i,rock in enumerate(dat['rock']['types']):
        if i != to_rocktype:
            if 'cells' in rock:
                for ci in cell_idx:
                    try:
                        rock['cells'].remove(ci)
                    except ValueError:
                        pass # ok if not in list
        else:
            if 'cells' in rock:
                rock['cells'] = sorted(list(set(rock['cells']) | set(cell_idx)))
            else:
                rock['cells'] = sorted(cell_idx)
