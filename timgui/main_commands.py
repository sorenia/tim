"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

"""All the commands for main window.

Add a new command by creating a new subclass of WindowCommand.  The name of
commands must end with "_command".  It is essential to implement the run()
method for the command, which is how the command will be executed.  Class
(static) properties such as "tip", and "caption" can be defined, useful to tell
user what the command is about.  Undoable commands should define the (optional)
undo() method.
"""

from base_commands import WindowCommand

from PyQt4.QtCore import *
from PyQt4.QtGui import *

import logging

class undo_command(WindowCommand):
    tip = "Undo (Ctrl+z)"
    caption = "Undo"
    def run(self):
        self.main_window.undoCommand()

class create_geo_command(WindowCommand):
    tip = "Create a new Geometry file"
    caption = "&New Geometry"
    def run(self):
        import os.path
        from ui_create_model import Ui_CreateMulgrid
        win = Ui_CreateMulgrid()
        win._current_path = self.main_window._current_path
        if win.exec_():
            if win.load_on_exit():
                self.main_window.model.loadMulgrid(win.geo)
        if os.path.exists(win._current_path):
            self.main_window._current_path = win._current_path
        del win

class create_dat_command(WindowCommand):
    tip = "Create a new TOUGH2 input file"
    caption = "Ne&w Data File"
    def run(self):
        geo = self.main_window.model.geo()
        if geo is None:
            QMessageBox.critical(None, 'Error',
                'Load a geometry file before creating input file.')
            return
        import os.path
        from ui_create_model import Ui_CreateT2Data
        win = Ui_CreateT2Data()
        win.geo = geo
        win._current_path = self.main_window._current_path
        if win.exec_():
            if win.load_on_exit():
                self.main_window.model.dat_simulator = 'autough2'
                self.main_window.model.loadDat(win.dat)
        if os.path.exists(win._current_path):
            self.main_window._current_path = win._current_path
        del win

class open_geo_command(WindowCommand):
    tip = "Open Geometry file (Ctrl+O)"
    caption = "&Open Geometry"
    def run(self, fname=None):

        ##### prev dat dirty, ask user if continue
        if self.main_window.model._dirty_dat:
            msg = "  ".join([
                "Current input data file has unsaved change(s).",
                "You will lose these edits if continue by clicking Discard.",
                "Otherwise click Abort to cancel loading.",
                ])
            ans = QMessageBox.question(
                None,
                "Current Input File (t2data) Has Unsaved Changes",
                msg,
                (QMessageBox.Discard | QMessageBox.Abort),
                QMessageBox.Abort)
            if ans == QMessageBox.Abort:
                return

        ok = False
        if self.main_window._current_path == '':
            self.main_window._current_path = '.'
        if fname is None:
            fname = QFileDialog.getOpenFileName(self.main_window,
                "Open (Mulgraph) Geometry File", self.main_window._current_path,
                "Geometry files (*.dat *.DAT)")
        else:
            fname = QString(fname)
        if not fname.isEmpty():
            self.main_window._current_path = QFileInfo(fname).path()
            self.main_window.statusBar().showMessage('Loading ' + str(fname) + ' ...')

            ok = self.main_window.model.loadFileMulgrid(str(fname))


            #self.main_window.model.setMulgridFilename(str(fname))
            #self.main_window.loading_thread = QThread()
            #self.main_window.connect(self.main_window.loading_thread,SIGNAL("started ()"),self.main_window.model.loadMulgrid)
            #self.main_window.model.moveToThread(self.main_window.loading_thread)
            #self.main_window.loading_thread.start()

        if ok:
            self.main_window.statusBar().showMessage('Geometry ' + str(fname) + ' is ready.')
        else:
            self.main_window.statusBar().clearMessage()
            if self.main_window.model.geo() is not None:
                self.main_window.statusBar().showMessage('Still using previously loaded geometry', 5000)


class open_dat_command(WindowCommand):
    tip = "Open TOUGH2 Data File (Ctrl+D)"
    caption = "Open &Data File"
    def run(self):
        ##### no geometry
        # adding default dat variables requires geo pre-loaded
        if self.main_window.model.geo() is None:
            msg = "Need to load Geometry file before loading data file."
            QMessageBox.critical(
                None,
                "Error Loading TOUGH2 Data File",
                msg)
            return

        ##### prev dat dirty, ask user if continue
        if self.main_window.model._dirty_dat:
            msg = "  ".join([
                "Current input data file has unsaved change(s).",
                "You will lose these edits if continue by clicking Discard.",
                "Otherwise click Abort to cancel loading.",
                ])
            ans = QMessageBox.question(
                None,
                "Current Input File (t2data) Has Unsaved Changes",
                msg,
                (QMessageBox.Discard | QMessageBox.Abort),
                QMessageBox.Abort)
            if ans == QMessageBox.Abort:
                return

        ok = False
        if self.main_window._current_path == '':
            self.main_window._current_path = '.'
        fname = QFileDialog.getOpenFileName(self.main_window,
                    "Open TOUGH2 &Data File", self.main_window._current_path)
        if not fname.isEmpty():
            self.main_window._current_path = QFileInfo(fname).path()
            self.main_window.statusBar().showMessage('Loading ' + str(fname) + ' ...')
            ok = self.main_window.model.loadFileDataInput(str(fname))

        if ok:
            ##### check if t2data matches geometry
            warn_mismatch = True # TODO: make this one of the setting
            if warn_mismatch:
                model = self.main_window.model
                try: #again, should this be a try/except or an if isinstance()
                    dat_blocks = model.dat().grid.num_blocks
                except AttributeError:
                    #WSUPPORT: need to find some way of checking that data file and geometry file are compatible
                    logging.info('Unable to check match between data file and geometry file: %s is a waiwera data file and cannot be validated' % fname)
                else:
                    if model.geo().num_blocks != dat_blocks:
                        from t2model import dat_info
                        msg = "\n\n".join([
                            "Warning, loaded Input data file does not appear to match the geometry file.",
                            "[mulgrid] object:",
                            str(model.geo()),
                            "[data] object:",
                            str(dat_info(model.dat())),
                            ])
                        QMessageBox.warning(
                            None,
                            "Number of blocks mismatch",
                            msg)
                        logging.warning(msg)

            self.main_window.statusBar().showMessage('Simulation input ' + str(fname) + ' is ready.')
        else:
            self.main_window.statusBar().clearMessage()
            if self.main_window.model.geo() is not None:
                self.main_window.statusBar().showMessage('Still using previously loaded input data file', 5000)


class open_lst_command(WindowCommand):
    tip = "Open TOUGH2 Listing File (Ctrl+L)"
    caption = "Open &Listing File"
    def run(self):
        ##### no geometry
        if self.main_window.model.geo() is None:
            msg = "Need to load Geometry file before loading listing file."
            QMessageBox.critical(
                None,
                "Error Loading TOUGH2 Listing File",
                msg)
            return

        ok = False
        if self.main_window._current_path == '':
            self.main_window._current_path = '.'
        fname = QFileDialog.getOpenFileName(self.main_window,
                    "Open TOUGH2 &Listing File", self.main_window._current_path)
        if not fname.isEmpty():
            self.main_window._current_path = QFileInfo(fname).path()
            self.main_window.statusBar().showMessage('Loading ' + str(fname) + ' ...')
            ok = self.main_window.model.loadFileT2listing(str(fname))

        if ok:
            ##### check if t2listing matches geometry
            warn_mismatch = True # TODO: make this one of the setting
            if warn_mismatch:
                model = self.main_window.model
                if model.geo().num_blocks != model.lst().element.num_rows:
                    from t2model import lst_info
                    msg = "\n\n".join([
                        "Warning, loaded TOUGH2 listing file does not appear to match the geometry file.",
                        "[mulgrid] object:",
                        str(model.geo()),
                        "[t2listing] object:",
                        str(lst_info(model.lst())),
                        ])
                    QMessageBox.warning(
                        None,
                        "Number of blocks mismatch",
                        msg)
                    logging.warning(msg)

            self.main_window.statusBar().showMessage('TOUGH2 listing ' + str(fname) + ' is ready.')
        else:
            self.main_window.statusBar().clearMessage()
            if self.main_window.model.geo() is not None:
                self.main_window.statusBar().showMessage('Still using previously loaded TOUGH2 listing file', 5000)


class save_dat_command(WindowCommand):
    tip = "Save TOUGH2 or waiwera Data File (Ctrl+S)"
    caption = "&Save Data File"
    def run(self):
        if self.main_window._current_path == '':
            self.main_window._current_path = '.'
        ##need to update this more so that it actually works with waiwera stuff
        try:
            orig_dat_fname = self.main_window.model.dat().filename
        except:
            orig_dat_fname = self.main_window.model.dat()['_metadata']['filename']
        # this static method uses system dialog, so the extensions may be hidden
        # because of user's system setting
        # TODO: find way to change this setting on Windows, use module _winreg
        # https://www.daniweb.com/hardware-and-software/microsoft-windows/threads/206594/how-to-programatically-hideshow-extensions-for-known-file-types
        # http://www.blog.pythonlibrary.org/2010/03/20/pythons-_winreg-editing-the-windows-registry/
        # may need to use pywin32 api to notify refresh
        # from win32com.shell import shell, shellcon
        # shell.SHChangeNotify()
        # try:
        #     from _winreg import OpenKey, QueryValueEx, SetValueEx, FlushKey
        #     from _winreg import HKEY_CURRENT_USER, KEY_ALL_ACCESS
        #     k = OpenKey(HKEY_CURRENT_USER,
        #         'Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Advanced',
        #         0, KEY_ALL_ACCESS)
        #     v,t = QueryValueEx(k, 'HideFileExt')
        #     print 'HideFileExt = ', v
        #     SetValueEx(k, 'HideFileExt', None, t, 1)
        #     FlushKey(k)
        # except:
        #     pass
        fname = QFileDialog.getSaveFileName(self.main_window,
            "Save TOUGH2/waiwera Input File", orig_dat_fname)
        if not fname.isEmpty():
            self.main_window.model.saveEditedDat(str(fname))
            self.main_window.statusBar().showMessage('TOUGH2/waiwera input is saved.')

class load_npy_eleme_command(WindowCommand):
    tip = "Load a .npy (numpy array file) to visualise as element variable"
    caption = "Load Block Variable (.npy)"
    def run(self):
        geo = self.main_window.model.geo()
        if geo is None:
            msg = 'Please load geometry file first.'
            QMessageBox.critical(None, 'Error', msg)
            return
        import numpy as np
        if self.main_window._current_path == '':
            self.main_window._current_path = '.'
        fname = QFileDialog.getOpenFileName(self.main_window,
                    "Open .npy (numpy array file) File", self.main_window._current_path)
        if not fname.isEmpty():
            try:
                import default_model_datatypes
                arr = np.load(str(fname))
                # if np.size(arr, 0) != geo.num_blocks or np.size(arr, 1) != 1:
                if np.size(arr) != geo.num_blocks:
                    raise Exception('.npy file have array with dimension %s mismatches geometry file (%i)' % (str(np.shape(arr)), geo.num_blocks))
                data = default_model_datatypes.Constant_Block_Data(values=arr, name=str(fname))
                self.main_window.model.registerData(data)
                self.main_window.statusBar().showMessage('%s is loaded.' % str(fname))
            except Exception as e:
                msg = 'Unable to load file as Constant Block Data: %s' % str(e)
                logging.error(msg)
                QMessageBox.critical(None, 'Error', msg)

class console_start_command(WindowCommand):
    tip = "Open Interactive Console"
    caption = "&Interactive Console"
    def run(self):
        self.main_window.console.start()

class draw_line_command(WindowCommand):
    tip = "Draw line for slice creation"
    caption = "Draw Slice Line"
    def run(self):
        from graphics_scenes import MyScene
        self.main_window.mode_control.setMode(MyScene.LINE_CREATION_MODE)
        self.main_window.dock_windows['Line'].show()
        self.main_window.dock_windows['Line'].raise_()


class create_slice_command(WindowCommand):
    """ TESTING: create a slice using the points defined in points_board. This
    can be tested by do the following in the interactive console:
    >>> win = window()
    >>> a = QObject()
    >>> QObject.connect(a, SIGNAL("run_cmd"),win.runCommand)
    >>> a.emit(SIGNAL("run_cmd"), "create_slice", {'i':99})
    """
    tip = "Create slice scene from line"
    caption = "Create Slice"
    def run(self, sliceline=None):
        if sliceline is None:
            sliceline = self.main_window.points_board.polyline()
        if len(sliceline) < 2:
            QMessageBox.warning(self.main_window,'Error','A line with at least two points required to create slice')
            #err = QMessageBox(QMessageBox.Warning,'Error','A line with at least two points required to create slice')
            #err.exec_()
            return
        # maybe add code to check column track
        name, ok = QInputDialog.getText(self.main_window,'New Slice Name','Please enter a name for the new slice',QLineEdit.Normal,'Slice')
        if ok:
            final_name = self.main_window.scene_manager.addSceneSlice(sliceline,name)
            if final_name is None:
                QMessageBox.warning(self.main_window,
                    'Failed to Create Slice',
                    'Failed to calculte slice from line.  Please redraw slice line if your line lies outside the grid area.',
                    )
                return
            if str(final_name) <> str(name).strip():
                QMessageBox.information(self.main_window,'Slice Name','%s created' % final_name)
        self.original_shown_scene = self.main_window.scene_control.setCurrentScene(final_name)
        self.created_scene_name = final_name

    def undo(self):
        logging.info('Deleting scene: %s' % self.created_scene_name)
        self.main_window.scene_manager.removeScene(self.created_scene_name)
        self.main_window.scene_control.setCurrentScene(self.original_shown_scene)

class pick_rock_source_mode_command(WindowCommand):
    tip = "Pick Source Block"
    caption = "Pick Source Block"
    def run(self):
        self.main_window.dock_windows['Edit Rocktypes'].show()
        self.main_window.dock_windows['Edit Rocktypes'].raise_()
        from graphics_scenes import MyScene
        self.main_window.mode_control.setMode(MyScene.SINGLE_SELECTION_MODE)

class pick_blocks_mode_command(WindowCommand):
    tip = "Pick Target Blocks"
    caption = "Pick Target Blocks"
    def run(self):
        self.main_window.dock_windows['Edit Rocktypes'].show()
        self.main_window.dock_windows['Edit Rocktypes'].raise_()
        from graphics_scenes import MyScene
        self.main_window.mode_control.setMode(MyScene.MULTI_SELECTION_MODE)

class apply_rock_command(WindowCommand):
    tip = "Copy Rocktype from Source to Target"
    caption = "Apply Rocktype"
    def run(self):
        self.main_window.selection_board.applyRocktype()

class select_blocks_command(WindowCommand):
    tip = "Select blocks by providing a list of block names"
    caption = "Select Blocks"
    def run(self, blocklist=[]):
        from timgui.graphics_scenes import SceneData
        for b in blocklist:
            self.main_window.selection_board.multiSelect(b, SceneData.BLOCK_ITEM, True)

class reset_scale_command(WindowCommand):
    tip = "Reset to model extent"
    caption = "Reset View"
    def run(self):
        self.main_window.main_view.resetScale()

class zoom_in_command(WindowCommand):
    tip = "Zoom in"
    caption = "Zoom In"
    def run(self):
        self.main_window.main_view.zoomIn()

class zoom_out_command(WindowCommand):
    tip = "Zoom out"
    caption = "Zoom Out"
    def run(self):
        self.main_window.main_view.zoomOut()

class focus_main_view_command(WindowCommand):
    tip = "Focus on the main view"
    caption = "Goto Main View"
    def run(self):
        self.main_window.main_view.activateWindow()
        self.main_window.main_view.setFocus(Qt.OtherFocusReason)

class focus_scene_list_command(WindowCommand):
    tip = "Focus on the scene list"
    caption = "Goto Scene List"
    def run(self):
        self.main_window.dock_windows['Scene'].show()
        self.main_window.dock_windows['Scene'].raise_()
        self.main_window.scene_control.LayerSliceSelector.activateWindow()
        self.main_window.scene_control.LayerSliceSelector.setFocus(Qt.OtherFocusReason)

class focus_color_list_command(WindowCommand):
    tip = "Focus on the color variable list"
    caption = "Goto Color List"
    def run(self):
        self.main_window.dock_windows['Scene'].show()
        self.main_window.dock_windows['Scene'].raise_()
        self.main_window.scene_control.ColorSelector.activateWindow()
        self.main_window.scene_control.ColorSelector.setFocus(Qt.OtherFocusReason)

class focus_text_list_command(WindowCommand):
    tip = "Focus on the text variablelist"
    caption = "Goto Text List"
    def run(self):
        self.main_window.dock_windows['Scene'].show()
        self.main_window.dock_windows['Scene'].raise_()
        self.main_window.scene_control.TextSelector.activateWindow()
        self.main_window.scene_control.TextSelector.setFocus(Qt.OtherFocusReason)

class focus_arrow_list_command(WindowCommand):
    tip = "Focus on the arrow variable list"
    caption = "Goto Arrow List"
    def run(self):
        self.main_window.dock_windows['Scene'].show()
        self.main_window.dock_windows['Scene'].raise_()
        self.main_window.scene_control.ArrowSelector.activateWindow()
        self.main_window.scene_control.ArrowSelector.setFocus(Qt.OtherFocusReason)

class find_command(WindowCommand):
    tip = "Focus on the find box"
    caption = "Search and goto item"
    def run(self):
        self.main_window.dock_windows['Scene'].show()
        self.main_window.dock_windows['Scene'].raise_()
        self.main_window.scene_control.find_box.activateWindow()
        self.main_window.scene_control.find_box.setFocus(Qt.OtherFocusReason)


class show_unit_controls_command(WindowCommand):
    tip = "Edit default units and time"
    caption = "Set Initial Time"
    def run(self):
        self.main_window.unit_control.show()
        self.main_window.unit_control.raise_()
        self.main_window.unit_control.activateWindow()

class show_plot_history_command(WindowCommand):
    tip = "Time history plots"
    caption = "History Plots"
    def run(self):
        self.main_window.plot_board_history.show()
        self.main_window.plot_board_history.raise_()
        self.main_window.plot_board_history.activateWindow()

class show_plot_depth_command(WindowCommand):
    tip = "Downhole plots"
    caption = "Downhole Plots"
    def run(self):
        self.main_window.plot_board_depth.show()
        self.main_window.plot_board_depth.raise_()
        self.main_window.plot_board_depth.activateWindow()

class show_plot_along_command(WindowCommand):
    tip = "Plots along x-axis (first axis)"
    caption = "Radial Plots"
    def run(self):
        self.main_window.plot_board_along.show()
        self.main_window.plot_board_along.raise_()
        self.main_window.plot_board_along.activateWindow()

class show_gener_table_command(WindowCommand):
    tip = "Generation Table"
    caption = "GENER Table"
    def run(self):
        self.main_window.gener_table.show()
        self.main_window.gener_table.raise_()
        self.main_window.gener_table.activateWindow()

class show_info_board_command(WindowCommand):
    tip = "Show Information"
    caption = "Show Information"
    def run(self):
        self.main_window.dock_windows['Info'].hide()
        def show():
            self.main_window.dock_windows['Info'].show()
            self.main_window.dock_windows['Info'].raise_()
        QTimer.singleShot(100,show)

class show_rocks_table_command(WindowCommand):
    tip = "RocktypesTable"
    caption = "ROCKS Table"
    def run(self):
        self.main_window.rock_table.show()
        self.main_window.rock_table.raise_()
        self.main_window.rock_table.activateWindow()

class show_colorbar_control_command(WindowCommand):
    tip = "Edit Colorbar"
    caption = "Edit Colorbar"
    def run(self):
        self.main_window.colorbar_control.show()
        self.main_window.colorbar_control.raise_()
        self.main_window.colorbar_control.activateWindow()

class show_block_name_command(WindowCommand):
    tip = "Show Block Names"
    caption = "Show Block Names"
    def run(self):
        self.main_window.scene_control.setCurrentText('Block Name')

class show_item_display_command(WindowCommand):
    tip = "Item display settings"
    caption = "Customise well display"
    def run(self):
        self.main_window.item_display.show()
        self.main_window.item_display.raise_()
        self.main_window.item_display.activateWindow()


class make_wells_blue_command(WindowCommand):
    tip = "Blue Well Label"
    caption = "Make Well Blue"
    def run(self):
        self.main_window.scene_manager.well_setting_default = {
            'Track': {'color': 'blue', 'visible': True},
            'Text': {'color': 'blue', 'visible': True},
            'Head': {'color': 'blue', 'visible': True},
        }
        # only need to update current scene, other scenes will be synced when
        # set to view
        s = self.main_window.main_view.scene()
        self.main_window.scene_manager.updateSceneSettings(s)

class well_text_larger_command(WindowCommand):
    tip = "Well Label Size Increase"
    caption = "Well Label Size +"
    def run(self):
        self.main_window.scene_manager.wellTextLarger()

class well_text_smaller_command(WindowCommand):
    tip = "Well Label Size Decrease"
    caption = "Well Label Size -"
    def run(self):
        self.main_window.scene_manager.wellTextSmaller()

class well_text_pos_toggle_command(WindowCommand):
    tip = "Toggle Well Label Position"
    caption = "Toggle Well Label Position"
    def run(self):
        for s in self.main_window.scene_manager.scenelist:
            s.toggleWellLabelPosition()

class well_text_show_command(WindowCommand):
    tip = "Show Well Names"
    caption = "Show Well Names"
    def run(self):
        self.main_window.scene_control.show['well_text'].setChecked(True)
        self.main_window.scene_control._updateGroupVisible()

class well_text_hide_command(WindowCommand):
    tip = "Hide Well Names"
    caption = "Hide Well Names"
    def run(self):
        self.main_window.scene_control.show['well_text'].setChecked(False)
        self.main_window.scene_control._updateGroupVisible()

class block_text_larger_command(WindowCommand):
    tip = "Increase Text Size"
    caption = "Font Size +"
    def run(self):
        self.main_window.scene_manager.blockTextLarger()

class block_text_smaller_command(WindowCommand):
    tip = "Decrease Text Size"
    caption = "Font Size -"
    def run(self):
        self.main_window.scene_manager.blockTextSmaller()

class select_inside_polygon_command(WindowCommand):
    """ TODO: this implementation is too deep and too inflexible. but need some
    new APIs to be able to achieve this. """
    tip = "Select Blocks Inside Polygon"
    caption = "Select Inside Polygon"
    def run(self):
        s = self.main_window.main_view.scene()
        if s:
            s.selectInsidePolygon()

class select_intersect_polyline_command(WindowCommand):
    """ TODO: this implementation is too deep and too inflexible. but need some
    new APIs to be able to achieve this. """
    tip = "Select Blocks Along Line"
    caption = "Select Along Line"
    def run(self):
        s = self.main_window.main_view.scene()
        if s:
            s.selectIntersectPolyline()

class toggle_advanced_rocktype_editor_command(WindowCommand):
    tip = "Toggle Advanced Options in Rocktype Editor"
    caption = "Toggle Advanced Rocktype Editor"
    def run(self):
        w = self.main_window.selection_board.apply_rock
        w.toggleAdvanced()

class show_colorbar_command(WindowCommand):
    tip = "Show/hide the colorbar displayed in scene"
    caption = "Toggle Colorbar"
    def run(self, show=True):
        if show:
            self.main_window.colorbar_view.show()
        else:
            self.main_window.colorbar_view.hide()

class open_settings_dir_command(WindowCommand):
    tip = "Open settings folder"
    caption = "Open Settings Folder"
    def run(self):
        import settings
        # QDesktopServices.openUrl(QUrl(settings.settingsDir()))
        QDesktopServices.openUrl(QUrl().fromLocalFile(settings.settingsDir()))

class open_log_file_command(WindowCommand):
    tip = "Open application log file"
    caption = "Open Log File"
    def run(self):
        import log
        QDesktopServices.openUrl(QUrl().fromLocalFile(log.getLogFilename()))

class about_command(WindowCommand):
    tip = "About TIM"
    caption = "About TIM"
    def run(self):
        self.main_window.about.show()
        self.main_window.about.raise_()
        self.main_window.about.activateWindow()

class about_qt_command(WindowCommand):
    tip = "About Qt"
    caption = "About Qt"
    def run(self):
        QMessageBox.aboutQt(self.main_window, '')

class online_help_command(WindowCommand):
    tip = "Open Online Documentation"
    caption = "Manual"
    def run(self):
        QDesktopServices.openUrl(QUrl("http://tim.readthedocs.io/en/latest/"))

class online_getting_started_command(WindowCommand):
    tip = "Open Online Documentation - Getting Started"
    caption = "Getting Started"
    def run(self):
        QDesktopServices.openUrl(QUrl("http://tim.readthedocs.io/en/latest/getting_started/"))

class online_howtos_command(WindowCommand):
    tip = "Open Online Documentation - HOWTOs"
    caption = "HOWTOs"
    def run(self):
        QDesktopServices.openUrl(QUrl("http://tim.readthedocs.io/en/latest/howto/"))

class report_issue_command(WindowCommand):
    tip = "Report Bugs or Suggestions"
    caption = "Report Issue Online"
    def run(self):
        QDesktopServices.openUrl(QUrl("https://bitbucket.org/angusyeh/tim/issues"))

class run_model_command(WindowCommand):
    tip = "Run the currently loaded model (Ctrl+R)"
    caption = "&Run Model"
    def run(self):
        from t2incons import t2incon
        import os
        SIMULATOR = 'AUTOUGH2_5_teaching.exe'
        SIMULATOR_PATH = ''  # empty string if the same folder as TIM

        def get_tim_location():
            """ get the real path of the 'timgui' module """
            import timgui
            d = os.path.dirname(timgui.__file__)
            # assuming tim located with timgui folder
            return os.path.realpath(os.path.join(d, os.pardir))

        def append_exe_path(p=SIMULATOR_PATH):
            """ append os.environ['PATH'] so that os.sytem can see SIMULATOR
            wherever it was launched, only if not already in os.environ["PATH"]
            """
            if not p:
                p = get_tim_location()
            if p not in os.environ["PATH"].split(os.pathsep):
                os.environ["PATH"] += os.pathsep + p

        if self.main_window.model.dat() is None:
            QMessageBox.warning(self.main_window, '',
                'You must load a model input file first.' +
                '\nNothing will be run.')
            return
        if self.main_window.model._dirty_dat:
            QMessageBox.warning(self.main_window, '',
                'Current input data file has unsaved changes.You must save the model first or load a new model.' +
                '\nNothing will be run.')
            return

        if self.main_window._current_path == '':
            self.main_window._current_path = '.'

        # allow user to skip using incon if dat has START
        use_inc = True
        if self.main_window.model.dat().start:
            ans = QMessageBox.question(
                None,
                "Do you want to load INCON file?",
                "Input file has 'START' keyword, INCON file is optional.\n" \
                "Do you want to use INCON file for your run?",
                (QMessageBox.Yes | QMessageBox.No),
                QMessageBox.No)
            if ans == QMessageBox.No:
                use_inc = False

        # tmp incon file '_.incon' in same dir as dat file
        datpath = os.path.dirname(self.main_window.model.dat().filename)
        inc_filename = datpath + '/_'
        if inc_filename[0].islower():
            inc_filename = inc_filename + '.incon'
        else:
            inc_filename = inc_filename + '.INCON'

        if use_inc:
            # load user selected incon, and reset writing it to '_.incon'
            fname = QFileDialog.getOpenFileName(self.main_window,
                "Open TOUGH2 INCON File", self.main_window._current_path)
            if not fname.isEmpty():
                if len(str(fname)) > 80:
                    QMessageBox.warning(self.main_window, '',
                        'Model files are saved in a path that is too long.' +
                        '\nPlease make sure path + filename together is less than 80 characters.' +
                        '\nNothing will be run.')
                    return
                try:
                    inc = t2incon(str(fname))
                    if inc.blocklist != [b.name for b in self.main_window.model.dat().grid.blocklist]:
                        raise Exception
                except Exception:
                    QMessageBox.warning(self.main_window, '',
                        'Selected INCON file does not match model input file!' +
                        '\nNothing will be run.')
                    return

                # if os.path.isfile(inc_filename):
                #     ok_cancel = QMessageBox()
                #     ok_cancel.setText("This will overwrite existing INCON file: %s" % inc_filename)
                #     ok_cancel.setInformativeText("Do you want to continue?")
                #     ok_cancel.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
                #     ok_cancel.setDefaultButton(QMessageBox.Ok)
                #     ok = ok_cancel.exec_()
                #     if ok == QMessageBox.Cancel:
                #         return
                #     logging.info("Reset INCON and overwriting file %s" % inc_filename)
                inc.write(inc_filename, reset=True)
        else:
            # remove '_.incon' so gurantee not using incon
            if os.path.isfile(inc_filename):
                os.remove(inc_filename)

        logging.info("Using simulator: %s" % SIMULATOR)
        self.main_window.statusBar().showMessage("Running simulator: %s..." % SIMULATOR)
        print inc_filename
        append_exe_path()
        self.main_window.model.dat().run(incon_filename=inc_filename,
            simulator=SIMULATOR, silent=False)
        self.main_window.statusBar().showMessage('Simulation Ended')

class batch_plot_command(WindowCommand):
    tip = "Plot line plots in batch and generate .pdf file."
    caption = "Batch Plot"
    def run(self):
        from batch_plot import batch_plot
        if self.main_window._current_path == '':
            self.main_window._current_path = '.'

        ## geometry
        if self.main_window.model.geo() is None:
            fname = QFileDialog.getOpenFileName(self.main_window,
                "Open Geometry File",
                self.main_window._current_path)
            if fname.isEmpty():
                QMessageBox.warning(self.main_window, 'Warning', 'Batch Plotting Cancelled')
                return
            geo_name = str(fname)
        else:
            geo_name = self.main_window.model.geo().filename

        ## plotlist json
        fname = QFileDialog.getOpenFileName(self.main_window,
            "Open Plot List JSON File",
            self.main_window._current_path,
            "JSON (*.json)")
        if fname.isEmpty():
            QMessageBox.warning(self.main_window, 'Warning', 'Batch Plotting Cancelled')
            return
        json_name = str(fname)

        ## one or more listing file names
        fnames = QFileDialog.getOpenFileNames(self.main_window,
                    "Open One or More Listing Files",
                    self.main_window._current_path)
        if len(fnames) == 0:
            QMessageBox.warning(self.main_window, 'Warning', 'Batch Plotting Cancelled')
            return
        lst_name_list = [str(fn) for fn in fnames]

        ## actual plotting
        try:
            outfilename = batch_plot(geo_name, lst_name_list, json_name)
            msg = 'Batch Plotting Done\nFile %s generated.' % outfilename
            QMessageBox.information(self.main_window,'', msg)
            self.main_window.statusBar().showMessage(msg)
        except IOError as e:
            msg = str(e) + '\n(Make sure the PDF file is closed so it can be overwritten.)' + '\nBatch Plotting Cancelled'
            QMessageBox.warning(self.main_window, 'File IO Error', msg)

class load_line_command(WindowCommand):
    tip = "Import line from file"
    caption = "Import line from file"
    def run(self, fname=None):
        ok = False
        if self.main_window._current_path == '':
            self.main_window._current_path = '.'
        if fname is None:
            fname = QFileDialog.getOpenFileName(self.main_window,
                "Open Line File", self.main_window._current_path,
                "(*.bln *.BLN)")
        else:
            fname = QString(fname)
        if not fname.isEmpty():
            self.main_window._current_path = QFileInfo(fname).path()
            self.main_window.statusBar().showMessage('Loading ' + str(fname) + ' ...')
            from timgui.file_format import bln_file
            fbln = bln_file(str(fname))
            self.main_window.points_board.loadPolyline(fbln.lines[0].line)

class load_geo_json_command(WindowCommand):
    tip = "Load GeoJSON and draw in current scene"
    caption = "Load GeoJSON objects"
    def run(self, fname=None):
        logging.info('load_geo_json_command()')
        if fname is None:
            fname = QFileDialog.getOpenFileName(self.main_window,
                "Open GeoJSON File", self.main_window._current_path,
                "GeoJSON (*.json *.JSON)")
        else:
            fname = QString(fname)
        logging.info(str(fname))
        if not fname.isEmpty():
            self.main_window._current_path = QFileInfo(fname).path()
            self.main_window.statusBar().showMessage('Loading ' + str(fname) + ' ...')
            from file_format import load_geojson, shape_to_qgraphics
            s = load_geojson(str(fname))
            items = shape_to_qgraphics(s)
            for it in items:
                self.main_window.main_view.scene().addItem(it)
                QCoreApplication.processEvents()

class export_variable_csv_command(WindowCommand):
    tip = "Export Block Variable to comma-separated values (.CSV) file"
    caption = "Export Block Variable (.csv)"
    def run(self, fname=None, vname=None):
        ## variable
        if vname is None:
            vname = self.main_window.scene_control.currentColor()
        try:
            values = self.main_window.model.getData(self.main_window.model.BLOCK, vname)
        except KeyError:
            msg = "Please select a valid variable (View as Color)"
            QMessageBox.critical(
                None,
                "Error Exporting Variable to .CSV File",
                msg)
            return
        ## file name
        if fname is None:
            fname = QFileDialog.getSaveFileName(self.main_window,
                "Export CSV File", vname+'.csv')
        else:
            fname = QString(fname)
        if fname.isEmpty():
            return
        ## xyz
        import numpy as np
        geo = self.main_window.model.geo()
        xyz = np.zeros((geo.num_blocks, 3))
        for i,b in enumerate(geo.block_name_list):
            if geo.column_name(b) not in geo.column:
                # single atmosphere block
                xyz[i,:2] = geo.centre
                xyz[i,2] = geo.layerlist[0].centre
            else:
                # normal blocks
                xyz[i,:] = geo.block_centre(geo.layer_name(b), geo.column_name(b))
        ## write csv
        self.main_window._current_path = QFileInfo(fname).path()
        self.main_window.statusBar().showMessage('Exporting ' + str(fname) + ' ...')
        logging.info('export_variable_csv_command(): %s into %s' % (vname, str(fname)))
        import csv
        with open(str(fname), 'w') as csvfile:
            writer = csv.writer(csvfile, dialect='excel')
            # delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL
            writer.writerow(['Easting', 'Northing', 'Elevation', vname])
            for i in range(geo.num_blocks):
                writer.writerow(list(xyz[i,:]) + [values[i]])





