"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

"""
The first part of this module mimics a Singleton class that centralises the
management of modes.

Any object/controls that wants to maintain syncronisation with others should
connect to the QObject instance returned by getModeManager().  Which also has two
bound methods: .setMode() and .currentMode()

Using this mechanism like this (generally in __init__):

from ui_control_mode import getModeManager
self.setMode(getModeManager().currentMode())
QObject.connect(self, SIGNAL('set_scene_mode'), getModeManager().setMode)
QObject.connect(getModeManager(), SIGNAL('mode_changed'), self.setMode)

"""

from PyQt4.QtCore import *
from PyQt4.QtGui import *

# create the real QObject and add _mode as property
_mode_manager = QObject()
_mode_manager._mode = 0

# adding these functions as bound methods by using types.MethodType(), see:
# http://stackoverflow.com/questions/972/adding-a-method-to-an-existing-object
def _currentMode(self):
    # print '+++++ _currentMode() +++++ returning', id(self), self._mode
    return self._mode

def _setMode(self, mode):
    # print '+++++ _setMode() +++++ ', id(self), self._mode
    if self._mode <> mode:
        self._mode = mode
        self.emit(SIGNAL("mode_changed"), mode)

import types
_mode_manager.currentMode = types.MethodType(_currentMode, _mode_manager)
_mode_manager.setMode = types.MethodType(_setMode, _mode_manager)

# This function mimics the normal instantiation of an object: getModeManager()
def getModeManager():
    """ obtain the mode manager objects, which can be connected to keep mode
    synced """
    # print '+++++ getModeManager() +++++ returning', id(_mode_manager), _mode_manager
    return _mode_manager


class Ui_SceneModeControl(QObject):
    """ controls the mode of the scene, A SceneManager should be connected,
    Note that this UI is actually an UI Factory + Container, not UI itself.
    A layout or list of buttons can be returned for inserting into real
    controls """
    def __init__(self, show=None, parent=None):
        super(Ui_SceneModeControl, self).__init__(parent)
        self._buttons = []
        self._buttons_dict = {}
        self._actions = []
        self._actions_dict = {}

        # group modes, make exclusive
        self.button_group = QButtonGroup()
        self.action_group = QActionGroup(self) # have parent, so get deleted properly

        from graphics_scenes import MyScene
        # These should match MyScene's modes
        ALLMODES = [
            (MyScene.BROWSE_MODE,'Browse Mode',":/mouse.png"),
            (MyScene.MARQUEE_ZOOM_MODE,'Zoom Mode',":/marquee_zoom.png"),
            (MyScene.SINGLE_SELECTION_MODE,'Pick Source Block',":/single_select.png"),
            (MyScene.MULTI_SELECTION_MODE,'Pick Target Blocks',":/multi_select.png"),
            (MyScene.LINE_CREATION_MODE,'Draw Line',":/create_line.png"),
            ]
        # (MyScene.MARQUEE_SELECTION_MODE,'Marquee Selection Mode',":/marquee_select.png"),
        # (MyScene.WALL_CREATION_MODE,'Wall Creation Mode',":/create_wall.png"),

        if show is None:
            to_show = [(m,s,i,True) for m,s,i in ALLMODES]
        elif isinstance(show,list):
            to_show = [(m,s,i,m in show) for m,s,i in ALLMODES]
        else:
            to_show = [(m,s,i,m == show) for m,s,i in ALLMODES]

        # add mode switching (toggle) buttons
        for m,s,i,shw in to_show:
            self._addMode(m,s,i,shw)

        # internal signals
        self.connect(self.button_group,SIGNAL("buttonClicked (int)"),self._setSceneMode)

        # make sure all mode control wigets are connected upon initialisation.
        # i.e. if one instance change mode, all others will change accordingly
        self.setMode(getModeManager().currentMode())
        QObject.connect(self, SIGNAL('set_scene_mode'), getModeManager().setMode)
        QObject.connect(getModeManager(), SIGNAL('mode_changed'), self.setMode)

    def _addMode(self,scene_mode,name,icon=None,show=True):
        button = QToolButton()
        button.setText(name[0])
        if icon is not None: button.setIcon(QIcon(icon))
        button.setCheckable(True)
        button.setToolTip(name)

        action = QAction(self)
        action.setText(name)
        if icon is not None: action.setIcon(QIcon(icon))
        action.setCheckable(True)
        action.setToolTip(name)

        from functools import partial
        self.connect(action, SIGNAL("triggered ()"), partial(self.setMode,scene_mode));

        # the first one being default
        if len(self.button_group.buttons()) == 0:
            button.setChecked(True)
            action.setChecked(True)
        self.button_group.addButton(button)
        self.action_group.addAction(action)
        self.button_group.setId(button,scene_mode)
        if not show:
            button.hide()
            action.setVisible(False)
        self._buttons.append(button)
        self._buttons_dict[scene_mode] = button
        self._actions.append(action)
        self._actions_dict[scene_mode] = action

    def layout(self):
        """ return a QBoxLayout that contains buttons """
        layout = QBoxLayout(QBoxLayout.LeftToRight)
        for b in self._buttons:
            layout.addWidget(b)
        return layout

    def buttonlist(self):
        """ return a list of buttons """
        return self._buttons
    def button(self):
        """ return a dict of buttons """
        return self._buttons_dict

    def actionlist(self):
        """ return a list of actions """
        return self._actions
    def action(self):
        """ return a list of actions """
        return self._actions_dict

    def _setSceneMode(self, mode):
        """ triggered internally by clicking button """
        self.emit(SIGNAL("set_scene_mode"),mode)

    def setMode(self,mode):
        """ this ONLY changes the buttons, called by other components """
        if mode == self.currentMode(): return
        self.button_group.button(mode).setChecked(True)
        self._actions_dict[mode].setChecked(True)
        self._setSceneMode(mode)

    def currentMode(self):
        return self.button_group.checkedId()


