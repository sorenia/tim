"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt4.QtCore import *
from PyQt4.QtGui import *

class Ui_InfoBoard(QWidget):
    def __init__(self, parent=None):
        super(Ui_InfoBoard, self).__init__(parent)
        self._values = {} # dict of key and QLabel

        # additional info that is depending on model
        self._model = None
        # custom func calls for additional value processing
        self._customFuncs = []

        self._layout = QGridLayout()

        self._addLabel('Geometry File')
        self._addLabel('Input File')
        self._addLabel('Listing File')
        self._addLabel('Block')
        self._addLabel('Coordinates')
        self._addLabel('Connection')

        self._whole_layout = QVBoxLayout(self)
        self._whole_layout.addLayout(self._layout)
        self._whole_layout.addStretch(1)

    def _addLabel(self,key,value=''):
        key_label, val_label = QLabel(key+':'), QLabel(value)
        key_label.setAlignment(Qt.AlignRight)
        val_label.setAlignment(Qt.AlignLeft)
        val_label.setWordWrap(True)

        r = self._layout.rowCount()
        self._layout.addWidget(key_label,r,0)
        self._layout.addWidget(val_label,r,1)
        self._values[key] = val_label

    def _updateLabel(self,key,value):
        if key not in self._values:
            self._addLabel(key)
        self._values[key].setText(str(value))
    def showMessage(self,key,value,shorten=None):
        if shorten is not None:
            if isinstance(shorten,int):
                value = value[:shorten]
            elif isinstance(shorten,str):
                value = shorten % value
            elif callable(shorten):
                value = shorten(value)
        if key in self._values:
            if str(value) == str(self._values[key].text()):
                return
        self._updateLabel(key,value)
        # currently this is very slow, needs more work
        for f in self._customFuncs:
            f()

    def _modelUpdated(self):
        """ refresh displayed values when model updated """
        for f in self._customFuncs:
            f()

    def connectModel(self,model):
        """ connects to model for additional information to be displayed """
        def short_name(s):
            import ntpath
            return ntpath.basename(s)
        self.short_name = short_name
        from functools import partial
        self._geo_name = partial(self.showMessage,'Geometry File',shorten=self.short_name)
        self._dat_name = partial(self.showMessage,'Input File',shorten=self.short_name)
        self._lst_name = partial(self.showMessage,'Listing File',shorten=self.short_name)

        if self._model:
            self.disconnect(self._model,SIGNAL("model_lst_step_changed"),self._modelUpdated)
            self.disconnect(self._model,SIGNAL("model_geo_reloaded"),self._geo_name)
            self.disconnect(self._model,SIGNAL("model_dat_reloaded"),self._dat_name)
            self.disconnect(self._model,SIGNAL("model_lst_reloaded"),self._lst_name)
            self.disconnect(self._model,SIGNAL("model_dat_renamed"),self._dat_name)
        self._model = model
        if self._model:
            self.connect(self._model,SIGNAL("model_lst_step_changed"),self._modelUpdated)
            self.connect(self._model,SIGNAL("model_geo_reloaded"),self._geo_name)
            self.connect(self._model,SIGNAL("model_dat_reloaded"),self._dat_name)
            self.connect(self._model,SIGNAL("model_lst_reloaded"),self._lst_name)
            self.connect(self._model,SIGNAL("model_dat_renamed"),self._dat_name)


        # use another thread to do the expensive work
        self._thread = QThread()
        self._worker = InfoWorker(self._model)
        self._worker.moveToThread(self._thread)
        self.connect(self, SIGNAL("get_dat_block_info"), self._worker.dat_block_info)
        self.connect(self, SIGNAL("get_lst_eleme_info"), self._worker.lst_eleme_info)
        self.connect(self, SIGNAL("get_lst_conne_info"), self._worker.lst_conne_info)
        self.connect(self._worker, SIGNAL("show_message"), self.showMessage)
        self._thread.start(QThread.IdlePriority)

        # define custom functions to be called
        def rock_property():
            b = str(self._values['Block'].text())
            self.emit(SIGNAL("get_dat_block_info"),b)
        def eleme_table():
            b = str(self._values['Block'].text())
            self.emit(SIGNAL("get_lst_eleme_info"),b)
        def conne_table():
            cc = str(self._values['Connection'].text())
            self.emit(SIGNAL("lst_conne_info"),cc)

        # add into list so will be called when ever showMessage is being called
        self._customFuncs += [rock_property, eleme_table, conne_table]

    def closeEvent(self, event):
        self._thread.quit()
        self._thread.wait()
        event.accept()

class InfoWorker(QObject):
    def __init__(self,model):
        super(InfoWorker, self).__init__()
        self._model = model
        self.last_block_dat = None
        self.last_block_lst = None
        self.last_conne = None

    def dat_block_info(self,b):
        if b:
            if self.last_block_dat == b:
                return
            else:
                self.last_block_dat = b
        else:
            self.emit(SIGNAL("show_message"),'Rock Type','')
            return
        try:
            r_name = self._model.dat().grid.block[b].rocktype.name
            self.emit(SIGNAL("show_message"),'Rock Type',r_name)
        except:
            self.emit(SIGNAL("show_message"),'Rock Type','')

    def lst_eleme_info(self,b):
        if self.last_block_lst == b: return
        self.last_block_lst = b
        try:
            n = 3 # default additional columns to print
            lst = self._model.lst()
            for v in lst.element.column_name[:n]:
                if b in lst.element.row_name:
                    self.emit(SIGNAL("show_message"),v,str(lst.element[b][v]))
                else:
                    self.emit(SIGNAL("show_message"),v,'')
        except:
            pass

    def lst_conne_info(self,cc):
        if self.last_conne == cc: return
        try:
            n = 3 # default additional columns to print
            lst = self._model.lst()
            c = eval(cc)
            for v in lst.connection.column_name[:n]:
                if c in lst.connection.row_name:
                    self.emit(SIGNAL("show_message"),v,str(lst.connection[c][v]))
                else:
                    self.emit(SIGNAL("show_message"),v,'')
        except:
            pass

if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)

    win = Ui_InfoBoard()
    win.show()

    win.showMessage('test','original')
    win.showMessage('test','kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk ')

    app.exec_()
