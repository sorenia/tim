"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt4.QtCore import *
from PyQt4.QtGui import *

from t2data import t2data_format_specification
from mulgrids import mulgrid
from graph_widgets import MplCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar

from functools import partial
import logging
from ui_pairs import QBtnPair, QEditPair

# Unable to use t2model.mulgrid directly here.  .rectangular() would use the
# original pytough mulgrid() anyway.  Here we binds our methods into mulgrid
# TODO, see if other better work around in sub-classing see
# https://stackoverflow.com/questions/6627387/how-do-i-bind-an-existing-
# instance-method-in-one-class-to-another-class
from t2model import mulgrid as tim_mulgrid
mulgrid.well_blocks = tim_mulgrid.well_blocks.im_func
mulgrid.column_blocks_indices = tim_mulgrid.column_blocks_indices.im_func

def shown_as_spec(line_edit, format_spec):
    """ this prettifies QLineEdit content to match how the value would be
    printed (usualy in TOUGH2 input file.  Hence it's better to give it what
    PyTOUGH specifies """
    v = float(line_edit.text())
    t = ('%'+format_spec) % v
    line_edit.setText(t)

def get_t2spec(section_key, name):
    names, specs = t2data_format_specification[section_key]
    i = names.index(name)
    return specs[i].replace('d', 'i')

def getLineEditSizeHint(n):
    e1 = QLabel('M'*n).sizeHint()
    e2 = QLineEdit().sizeHint()
    return QSize(max(e1.width(), e2.width()), max(e1.height(), e2.height()))

def check_float_spec(text, format_spec, width):
    """ return True if the float value can be 'recovered' once converted into
    spec, with fixed width. """
    tol = 0.01
    v0 = float(text)
    try:
        v1 = float((format_spec % v0)[:width])
        if v0 == 0.0:
            if v1 == v0:
                return v1, True
        if abs(v1-v0) / v0 < tol:
            return v1, True
        else:
            return None, False
    except Exception:
        return None, False

class QDoubleValidator_def(QDoubleValidator):
    """ QDoubleValidator that fixup empty QLineEdit to a default value """
    def __init__(self, *args, **kwargs):
        if 'default' in kwargs:
            self.default = kwargs['default']
            del kwargs['default']
        super(QDoubleValidator_def, self).__init__(*args)
        if 'bottom' in kwargs:
            self.setBottom(kwargs['bottom'])
        if 'top' in kwargs:
            self.setTop(kwargs['top'])
        if 'decimals' in kwargs:
            self.setDecimals(kwargs['decimals'])
    def validate(self, text, pos):
        try:
            v = float(text)
            if v < self.bottom() or v > self.top():
                return (QValidator.Intermediate, pos)
            return (QValidator.Acceptable, pos)
        except ValueError:
            return (QValidator.Intermediate, pos)
    def fixup(self, s):
        # change QString in place by replace everything
        s.replace(s, str(self.default))
    def setDefault(self, default):
        if default < self.bottom() or default > self.top():
            raise Exception('default value exceeds bounds')
        self.default = default

class T2FloatEdit(QLineEdit):
    """ Special QLineEdit that is designed for those t2data inputs.

    If user finishes editing with invalid text, will reset t0 default value.
    Also upon finish editing, the text will be set to comply with the format
    spec.
    """
    def __init__(self, **kwargs):
        """ arguments: default, top, bottom, spec
        """
        super(T2FloatEdit, self).__init__(None)
        v_kwargs = {k: kwargs[k] for k in kwargs.keys() if k in ['top', 'bottom', 'default']}
        self.setValidator(QDoubleValidator_def(**v_kwargs))
        if 'spec' in kwargs:
            self._spec = kwargs['spec']
            QObject.connect(self, SIGNAL('editingFinished ()'), self.preview)
        if 'default' in kwargs:
            self.setText(str(kwargs['default']))
        self.preview()
    def preview(self):
        v = float(self.text())
        if self._spec.startswith('%'):
            t = self._spec % v
        else:
            t = ('%'+self._spec) % v
        self.setText(t)
    def setDefault(self, default):
        self.validator().setDefault(default)
    def value(self):
        return float(self.text())


class Ui_EditRectMeshWidget(QWidget):
    """ A widget that edits inputs required for mulgrid.rectangular()

    To get the values required in the .rectangular() method, use method
    .getInputs(), which will return a dictionary with keys matching arguments
    names.

    TODO:
        Use a QStackedLayout or QStackedWidget for switching between
        the two implementations of creating a mulgrid, simple and indexed.

        (Tip: Apply layouts to a QWidget or a QFrame and create _setupOthers2
         with correct index)
    """
    def __init__(self, parent=None):
        super(Ui_EditRectMeshWidget, self).__init__(parent)
        self.setWindowTitle("Rectangular Mesh")
        # self.setSizeGripEnabled(True)

        ### control display of either Simple or Advanced grid creation
        # mode 1 is simple regular rectangular
        # mode 2 is irregular rectangular
        self.mode_selection = 1

        layout_mesh = QGridLayout()
        self._setupColumn('X:', layout_mesh, 1)
        self._setupColumn('Y:', layout_mesh, 2)
        self._setupColumn('Z:', layout_mesh, 3)
        self._setupOthers(layout_mesh)
        self.setLayout(layout_mesh)

        # self.edit_num[0].setValue(3)
        # self.edit_num[1].setValue(3)
        # self.edit_num[2].setValue(10)

        # Arrays for side lengths of mulgrid columns
        self.xlen = [100.,]
        self.ylen = [100.,]
        self.zlen = [100.,]
        #lengths tuple contains REFERENCES to the arrays
        self.lengths = (self.xlen,self.ylen,self.zlen)

        self.update()

    # Common methods between mode 1 and 2
    def _setupColumn(self, name, layout, col):
        if self.mode_selection == 1:
            self._setupColumn1(name, layout, col)
        elif self.mode_selection == 2:
            self._setupColumn2(name, layout, col)
    def update(self):
        if self.mode_selection == 1:
            self.update1()
        elif self.mode_selection == 2:
            self.update2()
    def getInputs(self):
        if self.mode_selection == 1:
            return self.getInputs1()
        elif self.mode_selection == 2:
            return self.getInputs2()


    def _setFixedSize(self, w):
        w.setMinimumSize(getLineEditSizeHint(10))
        w.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)

    def _setupColumn1(self, name, layout, col):
        if not hasattr(self, 'edit_origin'):
            self.edit_origin = []
            self.edit_size = []
            self.edit_num = []
            self.lbl_dimension = []
            self._edit_slot = []
            for i,t in enumerate(['Origin (m):',
                                 'Block size (m):',
                                 'Block count:',
                                 'Total dimension (m):',
                                 'Rotate (degree):',
                                 'Atmosphere:']):
                lbl = QLabel(t)
                lbl.setAlignment(Qt.AlignRight)
                layout.addWidget(lbl, i+1, 0)
        lbl_header = QLabel(name)
        lbl_header.setAlignment(Qt.AlignHCenter)
        self.edit_origin.append(QLineEdit())
        self.edit_size.append(QLineEdit())
        self.edit_num.append(QSpinBox())
        self.lbl_dimension.append(QLabel())
        self.lbl_dimension[-1].setAlignment(Qt.AlignHCenter)
        self._setFixedSize(self.edit_origin[-1])
        self._setFixedSize(self.edit_size[-1])
        self._setFixedSize(self.edit_num[-1])
        self._setFixedSize(self.lbl_dimension[-1])
        layout.addWidget(lbl_header, 0, col)
        layout.addWidget(self.edit_origin[-1], 1, col)
        layout.addWidget(self.edit_size[-1], 2, col)
        layout.addWidget(self.edit_num[-1], 3, col)
        layout.addWidget(self.lbl_dimension[-1], 4, col)

        ### rules/range etc.
        self.edit_origin[-1].setText('0.0')
        self.edit_size[-1].setText('1.0')
        # QDoubleValidator() can still endup with blank, implement fixup()
        self.edit_origin[-1].setValidator(
            QDoubleValidator_def(decimals=2, default=0.0))
        self.edit_size[-1].setValidator(
            QDoubleValidator_def(bottom=0.01, top=9999999.99, decimals=2, default=1.0))
        self.edit_num[-1].setRange(1, 1000);

        ### update to format spec
        f = partial(shown_as_spec, self.edit_size[-1], '10.2f')
        self._edit_slot.append(f)
        QObject.connect(self.edit_size[-1], SIGNAL('editingFinished ()'), self._edit_slot[-1])
        self._edit_slot[-1]()
        f = partial(shown_as_spec, self.edit_origin[-1], '10.2f')
        self._edit_slot.append(f)
        QObject.connect(self.edit_origin[-1], SIGNAL('editingFinished ()'), self._edit_slot[-1])
        self._edit_slot[-1]()

        QObject.connect(self.edit_origin[-1], SIGNAL('textChanged (const QString&)'), self.update)
        QObject.connect(self.edit_size[-1], SIGNAL('textChanged (const QString&)'), self.update)
        QObject.connect(self.edit_num[-1], SIGNAL('valueChanged (int)'), self.update)

    def update1(self):
        """ any user edits should call this to refresh UIs """
        ok = True
        # mesh dimension
        for i in range(3):
            try:
                size, size_ok = check_float_spec(self.edit_size[i].text(), '%10.2f', 10)
                if not size_ok:
                    raise ValueError
                origin, origin_ok = check_float_spec(self.edit_origin[i].text(), '%10.2f', 10)
                if not origin_ok:
                    raise ValueError
                if size < 0.01:
                    raise ZeroDivisionError
                txt = ('%10.2f' % (size * float(self.edit_num[i].text())))[:10]
            except ValueError:
                txt = 'Value needs to fit format: 10.2f'
                ok = False
            except ZeroDivisionError:
                txt = 'Positive block size required!'
                ok = False
            self.lbl_dimension[i].setText(txt)
        # rotation
        try:
            angle = float(self.edit_angle.text())
            if float(self.edit_angle.text()) < 0.0:
                txt = 'Negative for anti-clockwise'
            else:
                txt = 'Positive for clockwise'
        except ValueError:
            txt = 'Positive for clockwise'
            ok = False
        txt += ', valid between -45.0 and 45.0'
        self.lbl_angle.setText(txt)
        if ok:
            self.emit(SIGNAL("updated"))

    def getInputs1(self):
        """ return a dict of values for use in PyTOUGH's mulgrid.rectangular(),
        based on values entered by user
        """
        return {
            'xblocks': [float(self.edit_size[0].text())] * int(self.edit_num[0].text()),
            'yblocks': [float(self.edit_size[1].text())] * int(self.edit_num[1].text()),
            'zblocks': [float(self.edit_size[2].text())] * int(self.edit_num[2].text()),
            'origin': [float(e.text()) for e in self.edit_origin],
            'angle': float(self.edit_angle.text()),
            'atmos_type': self.edit_atm.currentIndex(),
        }

    def _setupColumn2(self, name, layout, col):
        """ An improved method for adding columns to a mulgrid

        This setup column uses buttons to add values to a list of lengths.
        The user can also insert and delete lengths at a specific index.
        """
        if not hasattr(self, 'edit_origin'):
            self.edit_size = []
            self.add_del = []
            self.ins_rmv = []
            self.index = []
            self.indexEdit = []
            self.lbl_dimension = []
            self.edit_origin = []
            self._edit_slot = []
            for i,t in enumerate([
                                 'Block size (m):',
                                 '',
                                 '',
                                 'Index:',
                                 'Index Size (m):',
                                 'Total dimension (m):',
                                 'Origin (m):',
                                 'Rotate (degree):',
                                 'Atmosphere:'
                                 ]):
                lbl = QLabel(t)
                lbl.setAlignment(Qt.AlignRight)
                layout.addWidget(lbl, i+1, 0)
        lbl_header = QLabel(name)
        lbl_header.setAlignment(Qt.AlignHCenter)
        self.edit_origin.append(QLineEdit())
        self.edit_size.append(QLineEdit())
        self.add_del.append(QBtnPair("Extend","Shrink"))
        self.ins_rmv.append(QBtnPair("Insert","Remove"))
        self.index.append(QSpinBox())
        self.indexEdit.append(QEditPair())
        self.lbl_dimension.append(QLabel())
        self.lbl_dimension[-1].setAlignment(Qt.AlignHCenter)
        self._setFixedSize(self.edit_origin[-1])
        self._setFixedSize(self.edit_size[-1])
        self._setFixedSize(self.add_del[-1])
        self._setFixedSize(self.ins_rmv[-1])
        self._setFixedSize(self.index[-1])
        self._setFixedSize(self.indexEdit[-1])
        self._setFixedSize(self.lbl_dimension[-1])
        layout.addWidget(lbl_header, 0, col)
        layout.addWidget(self.edit_size[-1], 1, col)
        layout.addWidget(self.add_del[-1], 2, col)
        layout.addWidget(self.ins_rmv[-1], 3, col)
        layout.addWidget(self.index[-1], 4, col)
        layout.addWidget(self.indexEdit[-1], 5, col)
        layout.addWidget(self.lbl_dimension[-1], 6, col)
        layout.addWidget(self.edit_origin[-1], 7, col)

        ### rules/range etc.
        self.edit_origin[-1].setText('0.0')
        self.edit_size[-1].setText('100.0')
        # QDoubleValidator() can still endup with blank, implement fixup()
        self.edit_origin[-1].setValidator(
            QDoubleValidator_def(decimals=2, default=0.0))
        self.edit_size[-1].setValidator(
            QDoubleValidator_def(bottom=0.01, top=9999999.99, decimals=2, default=1.0))
        self.index[-1].setRange(0,1000)
        self.indexEdit[-1].editBox.setValidator(
            QDoubleValidator_def(bottom=0.01, top=9999999.99, decimals=2, default=100.0))

        ### update to format spec
        f = partial(shown_as_spec, self.edit_size[-1], '10.2f')
        self._edit_slot.append(f)
        QObject.connect(self.edit_size[-1], SIGNAL('editingFinished ()'), self._edit_slot[-1])
        self._edit_slot[-1]()
        f = partial(shown_as_spec, self.edit_origin[-1], '10.2f')
        self._edit_slot.append(f)
        QObject.connect(self.edit_origin[-1], SIGNAL('editingFinished ()'), self._edit_slot[-1])
        self._edit_slot[-1]()

        QObject.connect(self.edit_origin[-1], SIGNAL('editingFinished()'), self.update2)
        QObject.connect(self.edit_size[-1], SIGNAL('textChanged (const QString&)'), self.update2)
        QObject.connect(self.index[-1], SIGNAL('valueChanged (int)'), self.update2)

        # Functions above use functools.partial -> look at changing lambda to partial
        self.updateL = lambda who="extend": self.updateClick(name,who)
        self.updateR = lambda who="shrink": self.updateClick(name,who)
        self.connect(self.add_del[-1], SIGNAL("left"), self.updateL)
        self.connect(self.add_del[-1], SIGNAL("right"), self.updateR)

        self.indexL = lambda who="insert": self.updateClick(name,who)
        self.indexR = lambda who="remove": self.updateClick(name,who)
        self.connect(self.ins_rmv[-1], SIGNAL("left"), self.indexL)
        self.connect(self.ins_rmv[-1], SIGNAL("right"), self.indexR)

        self.editWhere = lambda : self.editList(name)
        self.connect(self.indexEdit[-1],SIGNAL("RETURNED"),self.editWhere)

    def updateClick(self,name,who):
        """ This is a mode_selection=2 only routine """
        if who == "extend":
            self.addList(name)
        elif who == "shrink":
            self.delList(name)
        elif who == "insert":
            self.addList(name,index = True)
        elif who == "remove":
            self.delList(name,index = True)
        self.update2()

    def editList(self,name):
        """ This is a mode_selection=2 only routine """
        selfList,selfInd = self.getWhich(name)
        ind = self.index[selfInd].value()
        value = float(self.indexEdit[selfInd].getText())
        selfList[ind]=value
        self.update2()

    def addList(self,name,index=False):
        selfList,selfInd = self.getWhich(name)
        value = float(self.edit_size[selfInd].text())
        if not index:
            self.lengths[selfInd].append(value)
        else:
            ind = self.index[selfInd].value()
            self.lengths[selfInd].insert(ind, value)

    def delList(self,name,index=False):
        selfList,selfInd = self.getWhich(name)
        value = float(self.edit_size[selfInd].text())
        # skip if only one length left, we need at least one value
        if len(self.lengths[selfInd]) == 1:
            return
        if not index:
            del self.lengths[selfInd][-1]
        else:
            ind = self.index[selfInd].value()
            del self.lengths[selfInd][ind]

    def getWhich(self,name):
        if name == "X:":
            thisList = self.xlen
            thisInd = 0
        if name == "Y:":
            thisList = self.ylen
            thisInd = 1
        if name == "Z:":
            thisList = self.zlen
            thisInd = 2
        return thisList, thisInd

    def update2(self):
        """ any user edits should call this to refresh UIs """
        ok = True

        # mesh dimension
        for i in range(3):
            try:
                size, size_ok = check_float_spec(self.edit_size[i].text(), '%10.2f', 10)
                if not size_ok:
                    raise ValueError
                origin, origin_ok = check_float_spec(self.edit_origin[i].text(), '%10.2f', 10)
                if not origin_ok:
                    raise ValueError
                if size < 0.01:
                    raise ZeroDivisionError
                txt = ('%10.2f' % (sum(self.lengths[i])))[:10]
            except ValueError:
                txt = 'Value needs to fit format: 10.2f'
                ok = False
            except ZeroDivisionError:
                txt = 'Positive block size required!'
                ok = False
            self.lbl_dimension[i].setText(txt)

            # Disables delete button if there are no items to delete
            if not self.lengths[i]: self.add_del[i].not_clickable("right")
            else: self.add_del[i].clickable("right")

            # Updates the range at which the index QSpinBox can range from
            num_items = len(self.lengths[i])
            self.index[i].setRange(0, num_items-1)

            # Updates the QEditPair to the current value at the specified index
            ind = self.index[i].value()
            if not self.lengths[i]: num = None
            else: num = self.lengths[i][ind]
            self.indexEdit[i].setText(str(num))
            self.indexEdit[i].editBox.setValidator(
                QDoubleValidator_def(bottom=0.01, top=9999999.99, decimals=2, default=num))

        # rotation
        try:
            angle = float(self.edit_angle.text())
            if float(self.edit_angle.text()) < 0.0:
                txt = 'Negative for anti-clockwise'
            else:
                txt = 'Positive for clockwise'
        except ValueError:
            txt = 'Positive for clockwise'
            ok = False
        txt += ', valid between -45.0 and 45.0'
        self.lbl_angle.setText(txt)
        if ok:
            self.emit(SIGNAL("updated"))

    def getInputs2(self):
        """ return a dict of values for use in PyTOUGH's mulgrid.rectangular(),
        based on values entered by user

        list() notation ensures that only values are sent instead of references
        """
        return {
            'xblocks': list(self.xlen),
            'yblocks': list(self.ylen),
            'zblocks': list(self.zlen),
            'origin': [float(e.text()) for e in self.edit_origin],
            'angle': float(self.edit_angle.text()),
            'atmos_type': self.edit_atm.currentIndex(),
        }

    def _setupOthers(self, layout):
        self.edit_angle = QLineEdit()
        self.edit_angle.setText('0.0')
        self.edit_angle.setValidator(
            QDoubleValidator_def(bottom=-45.0, top=45.0, decimals=2, default=0.0))
        self.lbl_angle = QLabel('Positive for clockwise')
        self.edit_atm = QComboBox()
        self.edit_atm.addItems([
            'A single atmosphere block',
            'One atmosphere block over each column',
            'No atmosphere blocks', ])
        self.edit_atm.setCurrentIndex(0)  # default single atm block
        self._setFixedSize(self.edit_angle)
        layout.addWidget(self.edit_angle, 8, 1) #Change row index of this
        layout.addWidget(self.lbl_angle, 8, 2, 1, 2) # Change row index row index of this
        layout.addWidget(self.edit_atm, 9, 1, 1, 3) # Change row index of this
        QObject.connect(self.edit_angle, SIGNAL('textChanged (const QString&)'), self.update)
        QObject.connect(self.edit_atm, SIGNAL('currentIndexChanged (int)'), self.update)


def quick_layer_plot(geo, canvas):
    """ Simple plotting of layer plot using timgui.graph_widgets.MplCanvas,
    which is a matplotlib Figure Canvas using qt4agg backend.
    """
    verts = []
    for col in geo.columnlist:
        verts.append(tuple([tuple([p for p in n.pos]) for n in col.node]))
    import matplotlib.collections as collections
    cols = collections.PolyCollection(verts, facecolors=[], edgecolors='k')

    MARGIN_RATIO = 0.1
    fig = canvas.fig
    fig.gca().clear()
    fig.gca().add_collection(cols)#, autolim=True)
    bb = geo.bounds
    x, y = bb[1][0] - bb[0][0], bb[1][1] - bb[0][1]
    fig.gca().set_xlim(bb[0][0]-x*MARGIN_RATIO, bb[1][0]+x*MARGIN_RATIO)
    fig.gca().set_ylim(bb[0][1]-y*MARGIN_RATIO, bb[1][1]+y*MARGIN_RATIO)
    # fig.gca().axis('off')
    # fig.tight_layout()
    fig.gca().set_aspect('equal', 'datalim')
    # fig.gca().relim()
    # fig.gca().autoscale_view()
    fig.gca().grid(True)
    fig.gca().set_title('Top View')
    fig.gca().set_xlabel('X (m)')
    fig.gca().set_ylabel('Y (m)')

    canvas.draw()

def quick_lays_plot(geo, canvas):
    """ Simple plotting of layer structure using timgui.graph_widgets.MplCanvas,
    which is a matplotlib Figure Canvas using qt4agg backend.
    """
    x1, x2 = 0.0, 1.0
    verts = []
    for lay in geo.layerlist:
        verts.append(([(x1, lay.bottom), (x1, lay.top), (x2, lay.top), (x2, lay.bottom)]))
    import matplotlib.collections as collections
    cols = collections.PolyCollection(verts, facecolors=[], edgecolors='k')

    MARGIN_RATIO = 0.1
    fig = canvas.fig
    fig.gca().clear()
    fig.gca().add_collection(cols)#, autolim=True)
    bb = ((x1, geo.layerlist[-1].bottom), (x2, geo.layerlist[0].top))
    x, y = bb[1][0] - bb[0][0], bb[1][1] - bb[0][1]
    fig.gca().set_xlim(bb[0][0]-x*MARGIN_RATIO, bb[1][0]+x*MARGIN_RATIO)
    fig.gca().set_ylim(bb[0][1]-y*MARGIN_RATIO, bb[1][1]+y*MARGIN_RATIO)
    fig.gca().tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom='off',      # ticks along the bottom edge are off
        top='off',         # ticks along the top edge are off
        labelbottom='off') # labels along the bottom edge are off
    fig.gca().grid(axis='y')
    fig.gca().set_title('Layers')
    fig.gca().set_ylabel('Z (m)')
    fig.tight_layout(pad=1.) # adjust subplot so ylabel won't be hidden

    canvas.draw()


class Ui_CreateMulgrid(QDialog):
    """ Create geometry file and then close dialog.  To get the geometry object,
    use .geo property.

    .load_on_exit() returns a boolean to indicate that if user wish the
    self.geo object to be loaded and use in the main application.

    User will be prompted to pick a file name to save to.
    """
    def __init__(self, parent=None):
        USE_WORKER = False # experimental, still not perfect

        super(Ui_CreateMulgrid, self).__init__(parent)
        self.setWindowTitle("Create Rectangular Mulgrid")

        self.mesh_edit = Ui_EditRectMeshWidget()
        self.grid_preview = MplCanvas(self)
        self.lays_preview = MplCanvas(self)
        grid_preview_toolbar = NavigationToolbar(self.grid_preview, self)
        lays_preview_toolbar = NavigationToolbar(self.lays_preview, self)
        self.chk_load_geo = QCheckBox('Use in Main View')
        self.chk_load_geo.setChecked(True)
        save_geo = QPushButton('Save')
        save_geo.setToolTip('Save Mulgraph Geometry File')
        cancel = QPushButton('Cancel')
        self.lbl_summary = QLabel('')
        # avoid enter key cause users "unexpected" action
        save_geo.setAutoDefault(False)
        cancel.setAutoDefault(False)

        self.geo = None

        layout_edit = QHBoxLayout()
        layout_edit.addWidget(self.mesh_edit)
        layout_edit.addWidget(self.lbl_summary)
        layout_edit.addStretch()
        layout_preview = QHBoxLayout()
        layout_preview.addWidget(self.grid_preview,5)
        layout_preview.addWidget(self.lays_preview,1)
        layout_save = QHBoxLayout()
        layout_save.addStretch()
        layout_save.addWidget(self.chk_load_geo)
        layout_save.addWidget(save_geo)
        layout_save.addWidget(cancel)
        layout_toolbar = QHBoxLayout()
        layout_toolbar.addWidget(grid_preview_toolbar,5)
        layout_toolbar.addWidget(lays_preview_toolbar,1)
        layout = QVBoxLayout()
        layout.addLayout(layout_edit)
        layout.addLayout(layout_toolbar)
        layout.addLayout(layout_preview)
        layout.addLayout(layout_save)
        self.setLayout(layout)

        QObject.connect(save_geo, SIGNAL('clicked ()'), self.save)
        QObject.connect(cancel, SIGNAL('clicked ()'), self.reject)

        if not USE_WORKER:
            QObject.connect(self.mesh_edit, SIGNAL('updated'), self.preview_grid)
            self.preview_grid()
        else:
            QObject.connect(self.mesh_edit, SIGNAL('updated'), self.emit_mesh_setting)
            # use another thread to do the expensive work
            self._thread = QThread()
            self._worker = MeshCreateWorker()
            self._worker.moveToThread(self._thread)
            self.connect(self, SIGNAL("create_grid"), self._worker.create_grid)
            self.connect(self._worker, SIGNAL("message"), self.show_message)
            self.connect(self._worker, SIGNAL("mulgrid"), self.preview_grid_worker)
            self._thread.start(QThread.IdlePriority)
            self.emit_mesh_setting()

    def emit_mesh_setting(self):
        mesh = self.mesh_edit.getInputs()
        self.emit(SIGNAL("create_grid"), mesh)

    def show_message(self, msg):
        self.lbl_summary.setText(msg)

    def preview_grid_worker(self, geo):
        del self.geo
        self.geo = geo
        if self.geo is None:
            self.grid_preview.clear_fig()
            self.lays_preview.clear_fig()
        else:
            quick_layer_plot(self.geo, self.grid_preview)
            quick_lays_plot(self.geo, self.lays_preview)

    def preview_grid(self):
        mesh = self.mesh_edit.getInputs()
        if hasattr(self, 'prev_mesh'):
            if mesh == self.prev_mesh:
                return
        self.prev_mesh = mesh
        del self.geo
        self.lbl_summary.setText('Creating...')
        QCoreApplication.processEvents() # show the text above
        try:
            self.geo = mulgrid().rectangular(
                mesh['xblocks'], mesh['yblocks'], mesh['zblocks'],
                origin=mesh['origin'],
                atmos_type=mesh['atmos_type'],
                )
            self.geo.rotate(mesh['angle'], centre=mesh['origin'][:2])
            self.lbl_summary.setText(str(self.geo))
        except Exception as e:
            self.geo = None
            msg = str(e)
            QMessageBox.warning(
                        None,
                        "Failed to create mulgrid",
                        msg)
            self.lbl_summary.setText('Failed to create mulgrid: ' + str(e))
            self.grid_preview.clear_fig()
            self.lays_preview.clear_fig()
            return
        quick_layer_plot(self.geo, self.grid_preview)
        quick_lays_plot(self.geo, self.lays_preview)

    def load_on_exit(self):
        return self.chk_load_geo.isChecked()

    def save(self):
        """
        28/07/2017 11:49:05 a.m.
        NOTE/TODO: It seems cleaner if I chdir to working directory and then
        save the file.  So model objects have the shorter .filename property.
        But this does not work with the rest of things at the moment.
        """
        import os
        if self.geo is None:
            logging.error('Unable to create mulgrid!')
            QMessageBox.critical(
                None,
                "Error",
                "Unable to create mulgrid!")
            return None
        default_fname = 'gnewmodel.dat'
        if hasattr(self, '_current_path'):
            if os.path.exists(self._current_path):
                default_fname = os.path.join(str(self._current_path), default_fname)
        fname = QFileDialog.getSaveFileName(self,
            "Save Mulgrid Geometry File", default_fname)
        if fname.isEmpty():
            # user cancel, just return
            # logging.error('Create mulgrid file failed, need to save to a file!')
            return None

        try:
            path,fn = os.path.split(str(fname))
            self._current_path = path
            # cwd = os.getcwd()
            # os.chdir(path)
            self.geo.write(str(fname))
            # os.chdir(cwd)
        except Exception as e:
            logging.error(str(e))
            self.reject()
        self.accept()

class MeshCreateWorker(QObject):
    def create_grid(self, mesh):
        if hasattr(self, 'prev_mesh'):
            if self.prev_mesh == mesh:
                return
        self.prev_mesh = mesh
        self.emit(SIGNAL("message"), "Creating...")
        try:
            geo = mulgrid().rectangular(
                mesh['xblocks'], mesh['yblocks'], mesh['zblocks'],
                origin=mesh['origin'],
                atmos_type=mesh['atmos_type'],
                )
            geo.rotate(mesh['angle'], centre=mesh['origin'][:2])
            msg = str(geo)
        except Exception as e:
            geo = None
            msg = "Failed to create mulgrid: " + str(e)
        self.emit(SIGNAL("message"), msg)
        self.emit(SIGNAL("mulgrid"), geo)  # emits None if fail so empty figure

def create_dat_inc(geo, filename, parameter={}, multi={}, others={}):
    """ t2data creation, returns created t2data, t2incon objects """
    from t2grids import t2grid
    from t2data import t2data, t2generator
    import os
    # Create TOUGH2 input data file:
    dat = t2data()
    simul = 'AUTOUGH2.2'
    dat.title = ''
    dat.parameter.update(
        {'max_timesteps': 999,
         'tstop': 1.e14,
         'const_timestep': 1.e7,
         'print_interval': 999,
         'print_level': 3,
         'gravity': 9.81,
         'default_incons': [101.3e3, 20.]})
    # dat.start = True
    # Set MOPs:
    dat.parameter['option'][1] = 1
    dat.parameter['option'][11] = 2
    dat.parameter['option'][12] = 2
    dat.parameter['option'][16] = 5
    # set EOS
    dat.multi = {
        'eos': 'EW',
        'num_components': 1,
        'num_equations': 2,
        'num_phases': 2,
        'num_secondary_parameters': 6,
    }
    # add grid, from geo
    dat.grid = t2grid().fromgeo(geo)

    ##### update with user settings #####
    dat.parameter.update(parameter)
    dat.multi.update(multi)
    dep = parameter['default_incons']
    eos = multi['eos']
    #####################################

    # convert_...() will fail if empty filename
    dat.filename = filename
    dat.convert_to_AUTOUGH2(warn=False, MP=False, simulator=simul, eos=eos)
    # create incon
    inc = dat.grid.incons(tuple(dep))

    # RPCAP
    dat.relative_permeability['type'] = 1.0
    dat.relative_permeability['parameters'] = [0.5, 0.0, 1.0, 0.5]
    dat.capillarity['type'] = 1
    dat.capillarity['parameters'] = [0.0, 0.0, 1.0]
    # LINEQ, NOTE do this after convert to autough2 would help
    dat.lineq['type'] = 2
    dat.lineq['epsilon'] = 1e-11
    dat.lineq['max_iterations'] = 999
    dat.lineq['num_orthog'] = 100
    dat.lineq['gauss'] = 1

    # additional things like GENERs etc.
    # In my model.json I have things like { "HeatFlux": {"Default": 0.06} }
    model_settings = {}
    model_settings.update(others)

    ### add base heat geners
    if 'HeatFlux' in model_settings:
        if 'Default' in model_settings['HeatFlux']:
            heatflow = model_settings['HeatFlux']['Default']
            layername=geo.layerlist[-1].name
            for col in geo.columnlist:
                bname = geo.block_name(layername,col.name)
                gname = geo.block_name('99', col.name)
                gx = float(col.area) * float(heatflow)
                gen=t2generator(name=gname, block=bname, type='HEAT', gx=gx)
                if gx != 0.0:
                    dat.add_generator(gen)

    return dat, inc


class Ui_EditT2dataWidget(QWidget):
    def __init__(self, parent=None):
        super(Ui_EditT2dataWidget, self).__init__(parent)
        self.setWindowTitle("T2data Settings")

        self.edits, self.lbls, self.lbls2 = {}, {}, {}
        self._edit_slot = {} # keep func alive for signals to connect to
        self._layout = QGridLayout()

        self._n_cols = 4 # set to be longer than the the longest label

        self.eos_names, self.eos_settings, self.eos_deps, self.eos_tips = zip(*[
            ('EOS1 (EW)',  [1,2,2,6,'EW'],  [101.3e3,20.], ['P','T/S']),
            ('EOS2 (EWC)', [2,3,2,6,'EWC'], [101.3e3,20., 0.], ['P','T/S','PCO2']),
            ('EOS3 (EWA)', [2,3,2,6,'EWA'], [101.3e3, 0.,20.], ['P','X/S+10','T']),
            ])

        # note PyQt/Qt use -inf and inf as default bounds for double
        # this can be obtained by -float('inf') and float('inf')
        for k,t,default,cons,t2spec,desc,long_desc in [
            ('tstop', 'float',
                    1.e14, None,
                    get_t2spec('param2', 'tstop'),
                    'TIMAX',
                    'Time in seconds at which simulation should stop'),
            ('max_timesteps', 'int',
                    999, (-999,9999),
                    get_t2spec('param1', 'max_timesteps'),
                    'MCYC',
                    'Maximum number of time steps to be calculated'),
            ('print_interval', 'int',
                    999, (-999,9999),
                    get_t2spec('param1', 'print_interval'),
                    'MCYPR',
                    'printout will occur for every multiple of MCYPR steps'),
            ('const_timestep', 'float',
                    1.e7, (0.0, float('Inf')),
                    get_t2spec('param2', 'const_timestep'),
                    'DELTEN',
                    'Length of time steps in seconds'),
            ('gravity', 'float',
                    9.81, None,
                    get_t2spec('param2', 'gravity'),
                    'GF',
                    'Magnitude (m/sec2) of the gravitational acceleration vector'),
            ]:
            self._addSetting(k, t, default, cons, t2spec, desc, long_desc)
        self._setUpEOSEdits()

        self._setUiOthers2()
        self._insertEmptyRow() # this helps to have the column width fixed
        self.setLayout(self._layout)

    def _insertEmptyRow(self):
        """ add empty/hidden lbls with min width for pretty layout """
        n = self._layout.rowCount()
        for i in range(self._n_cols):
            w = QLabel()
            w.setMinimumSize(getLineEditSizeHint(18))
            w.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
            self._layout.addWidget(w, n, i)

    def _insertSepLine(self):
        """ insert a line in QGridLayout, for nicer looking """
        line = QFrame()
        line.setGeometry(QRect(QPoint(0., 0.), QSize(1., 1.)))
        line.setFrameShape(QFrame.HLine)
        line.setFrameShadow(QFrame.Sunken);
        n = self._layout.rowCount()
        self._layout.addWidget(line, n, 0, 1, -1) # extend to edge

    def _setUiOthers2(self):
        self._insertSepLine()

        lbl_heat_1 = QLabel('Base Heat Flux (W/m**2)')
        self.edit_heat = T2FloatEdit(default=0.0, spec='%10.3e')
        lbl_heat_2 = QLabel('Will be applied to the blocks at base of the model')
        n = self._layout.rowCount()
        self._layout.addWidget(lbl_heat_1, n, 0)
        self._layout.addWidget(self.edit_heat, n, 1)
        self._layout.addWidget(lbl_heat_2, n, 2, 1, -1)

    def _addSetting(self, key, vtype, default=None, vrange=None,
                    t2spec=None, desc='', long_desc=''):
        if vtype == 'int':
            w = QSpinBox()
            if vrange:
                w.setRange(vrange[0], vrange[1])
            w.setValue(default)
        elif vtype == 'float':
            if vrange is None: vrange = (-float('inf'), float('inf'))
            w = T2FloatEdit(default=default, top=vrange[1], bottom=vrange[0],
                            spec=t2spec)
        else:
            raise Exception('%s not implemented yet' % vtype)
        self.lbls[key] = QLabel("%s (%s)" % (desc, key))
        self.lbls[key].setAlignment(Qt.AlignRight)
        self.lbls2[key] = QLabel(long_desc)
        self.edits[key] = w
        n = self._layout.rowCount()
        self._layout.addWidget(self.lbls[key], n, 0)
        self._layout.addWidget(w, n, 1)
        self._layout.addWidget(self.lbls2[key], n, 2, 1, -1)

    def _setUpEOSEdits(self):
        self._insertSepLine()

        ### combobox for EOS choices
        self.edit_eos = QComboBox()
        self.edit_eos.addItems(self.eos_names)
        n = self._layout.rowCount()
        self._layout.addWidget(self.edit_eos, n, 0)
        QObject.connect(self.edit_eos, SIGNAL('currentIndexChanged (int)'), self._changeEOS)

        ### primary variables QLineEdits
        dep_spec = get_t2spec('default_incons', 'incon')
        n = self._layout.rowCount()
        self.edit_dep, self.lbl_dep = [], []
        for i in range(self._n_cols):
            self.lbl_dep.append(QLabel('DEP(%i)' % (i+1)))
            self.lbl_dep[-1].setAlignment(Qt.AlignHCenter)
            self._layout.addWidget(self.lbl_dep[-1], n, i)
            w = T2FloatEdit(default=-1.0, bottom=0.0, spec=dep_spec)
            self._layout.addWidget(w, n+1, i)
            self.edit_dep.append(w)

        ### initialises properly
        self.edit_eos.setCurrentIndex(0)
        self._changeEOS(0)

    def _changeEOS(self, ieos):
        """ hide or show DEP (primary variables) depending on selected EOS """
        for j in range(len(self.edit_dep)):
            n_dep = len(self.eos_deps[ieos])
            if j < n_dep:
                self.lbl_dep[j].setVisible(True)
                self.lbl_dep[j].setText(('DEP(%i): '%(j+1))+self.eos_tips[ieos][j])
                self.edit_dep[j].setVisible(True)
                self.edit_dep[j].setDefault(self.eos_deps[ieos][j])
                self.edit_dep[j].setText(str(self.eos_deps[ieos][j]))
                self.edit_dep[j].preview()
            else:
                self.lbl_dep[j].setVisible(False)
                self.edit_dep[j].setVisible(False)

    def getInputs(self):
        eos = self.edit_eos.currentIndex()
        n_dep = len(self.eos_deps[eos])
        multi = dict(zip([
            'num_components',
            'num_equations',
            'num_phases',
            'num_secondary_parameters',
            'eos'], self.eos_settings[eos]))
        parameter = {k: w.value() for k,w in self.edits.iteritems()}
        parameter['default_incons'] = [self.edit_dep[i].value() for i in range(n_dep)]
        others = {
            'HeatFlux': {'Default': self.edit_heat.value()},
        }
        return parameter, multi, others

class Ui_CreateT2Data(QDialog):
    """ Creates t2data and incon files.  t2data and t2incon objects can be
    obtained by .dat and .inc properties.

    .load_on_exit() returns a boolean to indicate that if user wish the self.dat
    object to be loaded and use in the main application.

    User will be prompted to pick a file name for t2data to save to.  Incon will
    be written to a file with the same base name.
    """
    def __init__(self, parent=None):
        super(Ui_CreateT2Data, self).__init__(parent)
        self.setWindowTitle("Create TOUGH2 Input File")
        layout = QVBoxLayout()
        self.setLayout(layout)

        self.geo, self.dat, self.inc = None, None, None

        self.t2data_edit = Ui_EditT2dataWidget()
        self.t2data_edit.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.chk_load_dat = QCheckBox('Use in Main View')
        self.chk_load_dat.setChecked(True)
        create_dat = QPushButton('Save')
        create_dat.setToolTip('Save AUTOUGH2 Input File')
        cancel = QPushButton('Cancel')
        # avoid enter key cause users "unexpected" action
        create_dat.setAutoDefault(False)
        cancel.setAutoDefault(False)

        layout.addWidget(self.t2data_edit)
        layout_save = QHBoxLayout()
        layout_save.addStretch()
        layout_save.addWidget(self.chk_load_dat)
        layout_save.addWidget(create_dat)
        layout_save.addWidget(cancel)
        layout.addLayout(layout_save)

        QObject.connect(create_dat, SIGNAL('clicked ()'), self.save)
        QObject.connect(cancel, SIGNAL('clicked ()'), self.reject)

    def load_on_exit(self):
        return self.chk_load_dat.isChecked()

    def save(self):
        """
        28/07/2017 11:49:05 a.m.
        NOTE/TODO: It seems cleaner if I chdir to working directory and then
        save the file.  So model objects have the shorter .filename property.
        But this does not work with the rest of things at the moment.
        """
        import os
        parameter, multi, others = self.t2data_edit.getInputs()
        if self.geo is None:
            logging.error('Create t2data file failed, no geometry loaded!')
            QMessageBox.critical(None, 'Error',
                'Create t2data file failed, no geometry loaded!')
            return None, None
        default_fname = 'newmodel.dat'
        if hasattr(self, '_current_path'):
            if os.path.exists(self._current_path):
                default_fname = os.path.join(str(self._current_path), default_fname)
        fname = QFileDialog.getSaveFileName(self,
            "Save TOUGH2 Input File", default_fname)
        if fname.isEmpty():
            # user cancel, just return
            # logging.error('Create t2data file failed, need to save to a file!')
            return None, None

        try:
            path,fn = os.path.split(str(fname))
            self._current_path = path
            # cwd = os.getcwd()
            # os.chdir(path)

            # lower file names to avoid trouble in Linux, also easier to check
            fn = fn.lower()
            if fn.endswith('.dat'):
                fn_base = fn[:-4]
            else:
                fn_base = fn
            fname_dat = fn_base + '.dat'
            fname_inc = fn_base + '.incon'
            self.dat, self.inc = create_dat_inc(self.geo, fname_dat,
                                                parameter, multi, others)
            # note fname_dat passed into creation function is only for internal
            # pytough things, not important externally as file

            full_fname_dat = os.path.join(path, fname_dat)
            self.dat.write(full_fname_dat)
            logging.info('T2DATA file %s written' % full_fname_dat)
            full_fname_inc = os.path.join(path, fname_inc)
            self.inc.write(full_fname_inc)
            logging.info('INCON file %s written' % full_fname_inc)

            # os.chdir(cwd)
        except Exception as e:
            logging.error('failed in Ui_CreateT2Data.save()')
            logging.error(str(e))
            self.reject()
        self.accept()


if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)

    win1 = Ui_CreateMulgrid()
    win1.show()

    if win1.exec_():
        geo = win1.geo
        print 'load_on_exit():', win1.load_on_exit()
    else:
        geo = None

    # win = Ui_CreateT2Data()
    # win.geo = geo
    # win.show()
    # if win.exec_():
        # print 'load_on_exit():', win.load_on_exit()
