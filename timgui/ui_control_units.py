"""
pretty much still testing
UI_UnitsControl class controls a dialog window for controlling units
currently only controls time offset
opened by buttons on the time dock and graphing windows
connections in main window to the command to open them

need to figure out what to do after closing dialog - what signals does it emit
"""
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from units import setTimeOffset, setDefaultUnit,timeOffset,defaultUnit,Unit

MAX_VAL = 1.0E20

class Ui_UnitsControl(QDialog):
    def __init__(self,parent=None):
        """
        Dialog allowing user to change initial time of the model. Originally intended to offer control over many unit defaults.
        """
        super(Ui_UnitsControl,self).__init__(parent)
        self._model = None

        self.setWindowTitle('Configure Time Offset')
        self.setModal(True)

        #create fields (time offset only)
        self.time_box = QGroupBox('Set Initial Time')
        self.time_label = QLabel('Model initial time: ')
        self.time_offset = QDoubleSpinBox()
        self.time_offset.setDecimals(2)
        self.time_offset.setMaximum(MAX_VAL)
        self.time_offset.setMinimum(MAX_VAL * -1)
        self.time_unit = QComboBox()
        self.time_unit.addItems(['years','days','seconds'])

        self.error_message = QLabel('')

        #set up simple layout for time box
        time_layout = QGridLayout()
        time_layout.addWidget(self.time_label,0,0)
        time_layout.addWidget(self.time_offset,0,1)
        time_layout.addWidget(self.time_unit,0,2)
        self.time_box.setLayout(time_layout)

        self.button_box = QDialogButtonBox(
            QDialogButtonBox.Ok | QDialogButtonBox.Cancel)

        #layout
        self.setLayout(QVBoxLayout())
        self.layout().addWidget(self.time_box)
        #self.layout().addWidget(self.reset)
        self.layout().addWidget(self.button_box)

        #populate form with current defaults
        self.getCurrentValues()

        #connections
        QObject.connect(self.button_box, SIGNAL('accepted()'), self.accept)
        QObject.connect(self.button_box, SIGNAL('rejected()'), self.reject)

        #self.connect(self.reset,SIGNAL("clicked ()"),self.getCurrentValues)
        self.accepted.connect(self.updateDefault)
        self.rejected.connect(self.getCurrentValues)


    def getCurrentValues(self):
        """displays current time offset in the default unit
        """
        timeUnit = defaultUnit('second') #get default time unit
        offset = timeOffset(timeUnit) #get current timeOffset in terms of default unit



        index = self.time_unit.findText(''.join([timeUnit,'s'])) #default time unit's index in combobox
        self.time_offset.setValue(offset) #update spinbox value
        self.time_unit.setCurrentIndex(index) #update combobox unit selected


    def updateDefault(self):
        timeVal = self.time_offset.value()
        timeUnit = self.time_unit.currentText()
        offset = ' '.join([str(timeVal),str(timeUnit)])
        #convert proposed offset to seconds before comparing due to issues with float inaccuracy
        newSec = Unit(offset).to('second').magnitude
        #check that a change has been made
        if newSec != timeOffset():
            setTimeOffset(offset)
            self.emit(SIGNAL("updatedTimeOffset"))

    def resetDefault(self):
        """remove the time offset, usually needed when the listing file is reloaded"""
        setTimeOffset(0)
        self.getCurrentValues()
        self.emit(SIGNAL("updatedTimeOffset"))
        #dp not emit signal here as all other refreshing happens automatically when listing file is reloaded

    def connectModel(self,model):
        if self._model:
            self.disconnect(self._model,SIGNAL("model_lst_reloaded"),self.resetDefault)
        self._model = model
        self.connect(self._model,SIGNAL("model_lst_reloaded"),self.resetDefault)

def testing():
    import sys
    app = QApplication(sys.argv)
    setTimeOffset('123 years')
    setDefaultUnit('second')
    form = Ui_UnitsControl()
    return app,form

#should have something listening for changes?

if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)
    form = Ui_UnitsControl()
    #form.accepted.connect(updateData)
    form.show()
    app.exec_()