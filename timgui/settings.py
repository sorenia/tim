"""
Centralise some setting management.

Roughly three kinds of settings are used through out TIM:

- UI geometry and state: users usually don't need to ever look at these.  At the
moment all are kept using IniFormat, so that I can easily exam it during
development.

- User preferences: these are settings that have more to do with contents.
Users may want to modify these settings from time to time.  They are in INI
format, which should be easy enough to edit.  Also comments can be left in there
to explain things.  Note The QSettings object should be untouched (no set
values), so that the comments can stay.

- JSON files: they are the files the contain structures that must be remembered.
Eg. a list of plots, or colorbars.  I use JSON file because they need to be read
and written by the code.  Occasionally users can modify them (easily human
readble).  Or can be generated easily from other source.

At the moment, all these lives in the same directory in user's HOME.

TODO: In the future, this will be expanded into projects, basically each project
have its own settings directory.  In that case, maybe another category of
settings will be needed, eg. a list of recent projects or a shared unit
definitions.
"""

"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt4.QtCore import *
from PyQt4.QtGui import *
import logging
import os.path
import json

def appSettings():
    """ return the QSettings object that stores settings that user do not need
    to interact with, eg. window size and position, dock widget states. """
    # organization, application = 'TIM', 'application_state' will make settings
    # ini file at:
    #     Windows: %APPDATA%\TIM\application_state.ini
    #     Unix and Mac OS X: $HOME/.config/TIM/application_state.ini
    return QSettings(QSettings.IniFormat,QSettings.UserScope,"TIM","application_state")

def userSettings():
    """ returns a QSettings object that stores user preferences, note that this
    is a different QSettings from the appSettings() """
    # from os.path import join
    # user_settings_filename = join(settingsDir(),'user_preference'+fileExt())
    user_settings_filename = settingsDir()+'/'+'user_preference'+settingsExt()
    return QSettings(user_settings_filename, QSettings.IniFormat)

def appSettingsFile():
    """ return path of the application settings file (.appSettings()) """
    return str(appSettings().fileName())

def settingsExt():
    """ the file extension used for all settings files, based on the QSettings
    returned by appSettings() """
    from os.path import splitext
    return splitext(appSettingsFile())[1]

def settingsDir():
    """ return the directory of the application settings file. Probably most
    settings should come here."""
    from os.path import dirname
    return str(dirname(appSettingsFile()))

def getSettingsPath(name, fallback=True):
    """ This is used to get a full path of given file name so that it will be
    located with all other setting files in the same directory.  By default,
    fallback is True, a path point to where the code is will be used if the
    fname does not exist in the settings directory.  It is useful to set
    fallback=False when getting files to write, because all settings should
    located together in the settings directory.  """

    fname = ''.join([
        settingsDir(),
        '/',
        name,
        ])
    if fallback:
        try:
            f = open(fname,'r')
            f.close()
        except IOError:
            from os.path import normpath, dirname
            fname = ''.join([
                normpath(dirname(__file__)),
                '/',
                name,
                ])
            logging.debug("Settings %s not found, use supplied file as default." % name)
    return fname

def getCodePath(name):
    from os.path import normpath, dirname
    fname = ''.join([
        normpath(dirname(__file__)),
        '/',
        name,
        ])
    return fname


# registerFormat is not implemented in PyQt, so cannot use:
# QSettings.registerFormat('.json', readJsonFile, writeJsonFile)


def safe_loading_json_file(filename, default_out,
    try_dirs=[
        (os.path.normpath(settingsDir()), 'User settings'),
        (os.path.normpath(getCodePath('')),'Default'),
        ],
    object_hook=None):
    """ This function tries to load a JSON file and return the de-serialised
    dictionary.  If something goes wrong (either file IO error or bad json
    content) it will try to load file from the next specified directory.  The
    specified list of directories should be a list of tuples (dir,
    'description').  If all failed, default_out will be returned.

    Search order:
        - user setting directory
        - timgui code directory
    """
    out = default_out
    # Only the first one could be overwritten by TIM
    user_fname = os.path.join(try_dirs[0][0], filename)
    base = os.path.splitext(os.path.basename(filename))[0]
    for d,msg in try_dirs:
        fname = os.path.join(d, filename)
        try:
            with open(fname, 'r') as f:
                logging.info('Reading file %s.' % fname)
                try:
                    out = json.load(f, object_hook=object_hook)
                    break
                except ValueError as e:
                    msg1 = 'Error loading json file %s' % fname
                    msg2 = '  ' + str(e)
                    msg3 = " ".join([
                        "Do you want to Reset %s settings?" % base,
                        "Your settings will be overwritten or cleared.",
                        "Click Abort to quit and keep the file for correction."
                        ])
                    logging.error(msg1)
                    logging.error(msg2)
                    reset_abort = QMessageBox.critical(
                        None,
                        "Error parsing %s settings file" % base,
                        "\n".join([msg1, "", msg2, "", msg3]),
                        (QMessageBox.Reset | QMessageBox.Abort),
                        QMessageBox.Abort)
                    if reset_abort == QMessageBox.Abort:
                        logging.error("User abort to avoid overwrite %s" % user_fname)
                        # Qt event loop hasn't started yet, quit from python
                        exit(1)
                    # TODO: maybe keep a copy of bad json, so user won't lose
                    # modified JSON in the case of making editing mistake.
        except IOError:
            logging.info('%s file %s does not exist' % (msg, fname))
    return out


