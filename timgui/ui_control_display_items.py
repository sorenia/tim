"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

#still not sure if I should be doing stuff from within the model or from within Ui lydiaaa
from PyQt4.QtCore import *
from PyQt4.QtGui import *

from tree_item_model import TreeItemModel, TreeItem
# from ui_control_colorbar import ColorDialogDelegate

#TODO: tidy this: cut and pasted from ui_control_colorbar apart from model.setData call.
class ColorDialogDelegate(QItemDelegate):
    def __init__(self, parent=None):
        super(ColorDialogDelegate, self).__init__(parent)

    def createEditor(self, parent, option, index):
        editor = QColorDialog(parent)
        return editor

    def setEditorData(self, editor, index):
        editor.setCurrentColor(QColor(index.model().data(index, Qt.EditRole).toString()))

    def setModelData(self, editor, model, index):
        model.setData(index, str(editor.currentColor().name()),Qt.EditRole)


class WellTreeModel(TreeItemModel):
    WELLS = 0
    def __init__(self):
        super(WellTreeModel, self).__init__()

    def data(self, index, role):
        if not index.isValid():
            return QVariant()
        item = index.internalPointer()
        if role == Qt.DisplayRole:
            return item.data + '\tcolor: ' + item.color
            # return ' '.join([item.data,item.color])
        elif role == Qt.CheckStateRole:
            return item.checkState()
        else:
            # other roles not implemented
            return QVariant()

    def setData(self,index,value,role=Qt.CheckStateRole):
        if not index.isValid():
            return False
        if role == Qt.CheckStateRole:
            item = index.internalPointer()
            item.setCheckState(value.toInt()[0])
            # self.emit(SIGNAL("dataChanged (const QModelIndex&,const QModelIndex&)"),index,index)
            # have to include parent's checked state, it might be affected
            self.emit(SIGNAL("dataChanged (const QModelIndex&,const QModelIndex&)"),self.parent(index),index)
            #this signal triggers syncSettingstoScene in Ui window, be careful when adding colour data
            return True
        if role == Qt.EditRole:
            item = index.internalPointer()
            item.setColor(value)
            #colour has been changed
            self.emit(SIGNAL("dataChanged (const QModelIndex&,const QModelIndex&)"),self.parent(index),index)



    def setUpWells(self, geo=None):
        """clears tree and populates with list of current wells. geo should be mulgrid object containing wells"""
        self.beginResetModel()
        self.clear()

        #check geo
        if geo:
            #add top level 'Wells' item
            self.rootItem.children.append(TreeItem('Wells'))
            wellsParent = self.rootItem.children[self.WELLS]

            wellsParent.add(TreeItem('Texts (All well labels)'))
            textParent = wellsParent[-1]

            for w in geo.welllist:
                well = TreeItem(w.name)
                wellsParent.add(well) #add well to tree

                x = TreeItem('Text')
                textParent.add(x, as_secret=True) #add control of this well's 'label' to main Text checkbox
                #add parts as children
                well.add(x)
                well.add(TreeItem('Track'))
                well.add(TreeItem('Head'))

                #TODO: colour

            #TODO: come back to GIS objects later
            #self.rootItem.children.append(TreeItem('GIS Objects'))

        self.endResetModel()

    def syncCurrentSettings(self,well_settings,default):
        """receives current well settings and updates well tree to reflect them"""
        #need this to be separate from setting up wells for future expansion of loading settings
        wellParent = None
        for it in self.rootItem.children:
            if it.data == 'Wells':
                wellParent = it

        #check that there are current wells before syncing settings
        if not wellParent:
            return

        self.beginResetModel()

        for w in wellParent.children:
            #skip children that are not wells
            if w.data.startswith('Texts'):
                continue

            wn = w.data
            try:
                settings = well_settings[wn]
            except KeyError:
                settings = {}

            #TODO: not sure if this is the best way of doing this - perhaps issues
            #if structure of settings changes in the future?
            #also maybe slow/inefficient/too heavy?

            textSettings = default['Text']
            if 'Text' in settings.keys():
                textSettings.update(settings['Text'])

            trackSettings = default['Track']
            if 'Track' in settings.keys():
                trackSettings.update(settings['Track'])

            headSettings = default['Head']
            if 'Head' in settings.keys():
                headSettings.update(settings['Head'])

            partSettings = [textSettings, trackSettings,headSettings]

            #TODO: possibly this isn't necessary, but possibly more efficient by eliminating one loop?
            #if all well elements are visible set the whole well to be visible and continue
            # if all([pSetting['visible'] for pSetting in partSettings]):
            #     w.setCheckState(Qt.Checked)
            #     continue

            #else set each element individually
            for idx, pSetting in enumerate(partSettings):
                if pSetting['visible']:
                    w.children[idx].setCheckState(Qt.Checked)
                else:
                    w.children[idx].setCheckState(Qt.Unchecked)

                w.children[idx].setColor(pSetting['color'])



        self.endResetModel()

    def getWellSetting(self):
        """returns well settings so that scene manager settings can be updated"""
        #TODO: this emits a very large dictionary as it overwrites all settings including those that were defaults
        #change to only emit settings that are not default
        s = {}
        for it in self.rootItem.children:
            if it.data == 'Wells':
                wells = it
        for it in wells:
            #skip the Texts parent item
            if it.data.startswith('Texts'): continue
            s[it.data] = {}
            for c in it.children:
                s[it.data][c.data] = {}
                if c.checkState() == Qt.Checked:
                    s[it.data][c.data]['visible'] = True
                else:
                    s[it.data][c.data]['visible'] = False

                s[it.data][c.data]['color'] = c.color
        return s


class Ui_ItemDisplay(QDialog):
    """Dialog responsible for selectively showing/hiding wells,
    well features (head, track, label) and setting well colour"""

    def __init__(self,parent=None):
        super(Ui_ItemDisplay, self).__init__(parent)

        #set up variables
        self._model = None
        self._s_manager = None


        self.setWindowTitle('Feature Display Settings')

        #set up widgets
        tree_label = QLabel("Select Wells/Items to show:")
        self.tree = QTreeView()
        self.treeModel = WellTreeModel()
        self.tree.setModel(self.treeModel)
        self.tree.setItemDelegateForColumn(0,ColorDialogDelegate())

        #hide header as currently only using one column in tree
        self.tree.header().hide()

        #label position widgets
        label_positions = ['Well Head', 'Well Tail']
        self.label_buttons = QButtonGroup(self)
        label_ll = QLabel("Label Position (applies to all):")

        layout_ll = QBoxLayout(QBoxLayout.TopToBottom)
        layout_ll.addWidget(label_ll)
        for pos in label_positions:
            b = QRadioButton(pos)
            if pos == 'Well Head':
                b.setChecked(True)
            self.label_buttons.addButton(b)
            layout_ll.addWidget(b)

        QObject.connect(self.label_buttons, SIGNAL("buttonClicked (int)"), self.updateLabelPos)


        #layouts
        layout = QBoxLayout(QBoxLayout.TopToBottom)
        layout.addWidget(tree_label)
        layout.addWidget(self.tree)
        layout.addLayout(layout_ll)

        self.setLayout(layout)

        self.treeModel
        self.connect(self.treeModel,SIGNAL("dataChanged(QModelIndex,QModelIndex)"),self.syncSettingsToScene)

    def syncSettingsToScene(self):
        newSettings = self.treeModel.getWellSetting()

        if self._s_manager and newSettings:
            #send to scene manager
            self._s_manager.updateWellSettings(newSettings)


    def refreshTreeItems(self):
        geo = self._model.geo()

        #want to call even if geo is None as then tree should be wiped
        self.treeModel.setUpWells(geo)

        self.refreshTreeSettings()

    def refreshTreeSettings(self):
        if self._s_manager:
            settings = self._s_manager.well_setting
            default = self._s_manager.well_setting_default
            self.treeModel.syncCurrentSettings(settings, default)

            #expand top level items automatically
            rowCount = self.treeModel.rowCount(QModelIndex())
            for r in range(rowCount):
                index = self.treeModel.index(r,0,QModelIndex())
                self.tree.expand(index)


    def connectModel(self, model):
        if self._model:
            self.disconnect(self._model, SIGNAL("model_geo_reloaded"), self.refreshTreeItems)

        self._model = model

        if self._model:
            self.connect(self._model, SIGNAL("model_geo_reloaded"), self.refreshTreeItems)
            self.refreshTreeItems()



    def connectSceneManager(self, s_manager):
        """ connects to a scene manager """

        #don't care about any signals from scene manager at the moment so don't need to disconnect anythin
        #if self._s_manager is not None:

        #store scene manager
        self._s_manager = s_manager

        #update tree's current settings to match scene manager's settings
        self.refreshTreeSettings()

        #make sure radio buttons match scene_manager's current well_label settings
        pos = s_manager.well_label_pos
        if pos:
            bText = 'Well Head'
        else:
            bText = 'Well Tail'

        for b in self.label_buttons.buttons():
            if b.text() == bText:
                b.setChecked(True)


    def updateLabelPos(self, idx=None):
        #TODO: currently just assuming that button will stay in sync with scene_manager well_label position
        #could create and connect a signal for changing well_label_pos
        #but don't want to right now because its a small feature and now commands exist currently anyway
        clicked_text = self.label_buttons.button(idx).text()


        if not self._s_manager: return

        if clicked_text == 'Well Head':
            pos = True
        else:
            pos = False

        #do nothing if button was previously selected
        if pos == self._s_manager.well_label_pos: return

        self._s_manager.setWellLabelPos(pos)




if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)

    from t2model import Model

    model = Model()
    geoName = 'D:\\waiwera_files\\waiwera_example\\gwai41457_18.dat'
    model.loadFileMulgrid(geoName)


    test = Ui_ItemDisplay()
    test.connectModel(model)
    test.show()
    app.exec_()