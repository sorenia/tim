"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

import logging

def string_to_filename(s, nice=True):
    """ Normalise a string to be used as filename. All will be lower case, white
    sapce to be removed, and removal of unsuitable symbols.  This is intended to
    be friendly for all OS.  If nice is True, white spaces will be changed into
    underscores. """
    if nice:
        import re
        fn = re.sub(' +','_',s)
    else:
        fn = s
    return ''.join( x for x in fn if (x == '_'  or x.isalnum()) ).lower()

def line_line_intersection(line1, line2):
    """ return a point where two line intersects """
    [[x1,y1],[x2,y2]],[[x3,y3],[x4,y4]] = line1, line2
    den = (x1-x2)*(y3-y4) - (y1-y2)*(x3-x4)
    if den == 0.:
        # lines are parallel
        return None
    num1 = x1*y2-y1*x2
    num2 = x3*y4-y3*x4
    px = (num1*(x3-x4) - (x1-x2)*num2) / den
    py = (num1*(y3-y4) - (y1-y2)*num2) / den
    return [px,py]

def normal(p1,p2):
    import numpy as np
    """ return a normal vector of a line segment of two points """
    #     ^  normal() direction
    #     |
    # p1----p2
    return np.array([p1[1]-p2[1],p2[0]-p1[0]])

def line_point_direction(line,p3):
    """ return the value of (p3-p1) dot normal(line), if return 0.0,
    either p3 on line, or line segment has length zero """
    import numpy as np
    [p1,p2] = line
    return np.dot(p3-p1,normal(p1,p2))

def rockname_to_vals(rocknamelist,onlyshowrocks=None,rockgroup=None):
    """ setup rocktype values for color plots, either layer or slice.
    rocknamelist is a list of names (eg. given as the order of blocks in
    a slice plot.  will return a list of values smae length as rocknamelist,
    contains indices of the second returned list of rockname (grouped and
    sorted) if onlyshowrocks is provided as a list of either index or
    name strings, rocknames outside this list will be given a vlaue of -1.
    if rockgroup is provided, the grouping will be applied. """
    if onlyshowrocks:
        if isinstance(onlyshowrocks[0],int):
            rocknames = [rocknamelist[irock] for irock in onlyshowrocks]
        else:
            rocknames = onlyshowrocks
    else:
        rocknames = rocknamelist

    if rockgroup:
        if isinstance(rockgroup,str): rockgroup = [i for i,c in enumerate(rockgroup) if c<>'*']
        def namegroup(name): return ''.join([c if i in rockgroup else '*' for i,c in enumerate(name)])
        rocknames = [namegroup(name) for name in rocknames]

    rocknames = list(set(rocknames))
    rocknames.sort()
    rockmap = dict(zip(rocknames, range(len(rocknames))))

    if rockgroup:
        vals = []
        for name in rocknamelist:
            if namegroup(name) in rockmap:
                vals.append(rockmap[namegroup(name)])
            else:
                vals.append(-1)
    else:
        vals = [rockmap[name] for name in rocknamelist]
    return vals,rocknames

def merge_name(orig, new, convention):
    """ all should be 5 chars long, and new can contain '+' """
    final = ''
    for i,c in enumerate(convention):
        if c <> '+':
            final += new[i]
        else:
            final += orig[i]
    return final

def update_block_rock_property(dat, blk_names, to_rocktype, convention='*****'):
    """ Updates blocks (names listed in blk_names) to have rocktype (named
    to_rocktype).  The default convention means a simple assignment.  If '+' is
    used in parts of convention, a block's original rocktype name at that
    position will be preserved.  If this new rocktype name does not refer to any
    existing rocktype in dat.grid, a new rocktype will be created in dat.grid by
    copying properties of dat.grid.rocktype[to_rocktype].  Generally this
    changes all target blocks' permeability etc to the one specified. """
    for b in blk_names:
        r_name = merge_name(dat.grid.block[b].rocktype.name, to_rocktype, convention)
        if r_name not in dat.grid.rocktype:
            from copy import deepcopy
            new_rock = deepcopy(dat.grid.rocktype[to_rocktype])
            new_rock.name = r_name

            dat.grid.add_rocktype(new_rock)
            print ' new rocktype added: ', new_rock.name
            dat.grid.block[b].rocktype = new_rock
        else:
            dat.grid.block[b].rocktype = dat.grid.rocktype[r_name]
            # TODO: check if block new properties are the same as to_rocktype

def update_block_rock_name(dat, blk_names, to_rockname, convention='*****'):
    """ Updates blocks (names listed in blk_names) to use rocktype named
    to_rocktype.  The default convention means chaning rocktype name of blocks.
    If '+' is used in parts of convention, a block's original rocktype name at
    that position will be preserved.  If this new rocktype name does not refer
    to any existing rocktype in dat.grid, a new rocktype will be created in
    dat.grid by copying properties of that block's original rocktype.  Generally
    this does not affect the blocks' permeability etc.  Note if to_rockname
    exists indat.grid, the blocks will have rocktype/property refer to that
    rocktype, which may not have the same property as block's original
    one. """
    for b in blk_names:
        r_name = merge_name(dat.grid.block[b].rocktype.name, to_rockname, convention)
        if r_name not in dat.grid.rocktype:
            from copy import deepcopy
            new_rock = deepcopy(dat.grid.block[b].rocktype)
            new_rock.name = r_name

            dat.grid.add_rocktype(new_rock)
            print ' new rocktype added: ', new_rock.name
            dat.grid.block[b].rocktype = new_rock
        else:
            dat.grid.block[b].rocktype = dat.grid.rocktype[r_name]
            # TODO: check if block new properties are the same as original

def update_rocktype_byname(dat, blk_names, to_rockname, configuration=None):
    """
        if need new rocktype, new one will be created from scratch,
        according to rules in configuration.  Still needs some rocktype
        to copy from, a cloest match will be search and priorities
        the leading characters.
    """
    pass

def closest_rocktype(dat, to_rockname):

    pass

def update_rocktype_property_byblocks(prop,blk_names,dat,value):
    """ attempt to update one of the property of a list of blocks (names)
        by creating a new rocktype for them.  New rock type will be based
        on the first block's rocktype NOTE prop is a string used directly
        on PyTOUGH's rocktype object """
    rock = dat.grid.block[blk_names[0]].rocktype

    import copy
    new_rock = copy.deepcopy(rock)
    used_names = dat.grid.rocktype.keys()
    new_rock.name = find_unused_name(used_names, blk_names[0])
    dat.grid.add_rocktype(new_rock)
    rock = new_rock

    # prop is a string
    # if prop is 'porosity' then the line will be executed:
    #     rock.porosity = value
    exec 'rock.' + prop.strip() + ' = value'
    for b in blk_names:
        dat.grid.block[b].rocktype = rock

def update_rocktype_property_byblock(prop,blk_name,dat,value):
    """ attempt to update one of the property of a single block by creating
        a new rocktype for it if necessary.  NOTE prop is a string used
        directly on PyTOUGH's rocktype object """
    rock = dat.grid.block[blk_name].rocktype

    if dat.grid.rocktype_frequency(rock.name) > 1:
        import copy
        new_rock = copy.deepcopy(rock)
        used_names = dat.grid.rocktype.keys()
        new_rock.name = find_unused_name(used_names, blk_name)
        dat.grid.add_rocktype(new_rock)
        rock = new_rock

    # prop is a string
    # if prop is 'porosity' then the line will be executed:
    #     rock.porosity = value
    exec 'rock.' + prop.strip() + ' = value'
    dat.grid.block[blk_name].rocktype = rock
    print blk_name, 'used rocktype: ', rock.name

def valid_t2_name(s):
    """checks if a string is valid for use as a t2 name (eg for generator). A name is valid
    if it is exactly 5 characters long and its last 2 characters can be converted to int"""
    if len(s) != 5: return False
    if s[3:5].strip() == '': return False #last two characters can't both be spaces
    import re
    #last 2 characters of name must be able to be converted to int
    valid= re.compile('...[0-9 ]{2}')
    if valid.match(s) is not None:
        return True
    else:
        return False


def find_unused_name(used_names, suggestion):
    """ increment the name sug until the name is not used by
        any other rocktype """
    all_chars = ''.join([ ' 0123456789',
        'abcdefghijklmnopqrstuvwxyz',
        'ABCDEFGHIJKLMNOPQRSTUVWXYZ' ])
    sug = suggestion
    width = len(suggestion)
    # try:
    while sug in used_names:
        n = str_base_num(sug,all_chars)
        n += 1
        sug = num_base_str(n,all_chars,width)
    # except:
    #     raise Exception("Error! Can't 'find_unused_name()'")
    return sug

def str_base_num(s, chars='abcdefghijklmnopqrstuvwxyz'):
    base = len(chars)
    num = 0
    for p,c in enumerate(s[::-1]):
        if c not in chars: raise Exception("str_base_num() failed, '%s' not in base chars." % c)
        i = chars.index(c)
        if p == 0:
            num += i
        else:
            num += pow(base,p) * i
    return num

def num_base_str(x, chars='abcdefghijklmnopqrstuvwxyz', width=5):
    """ creates a character-based unique identifier from a given integer,
        both chars and width are customisable, output is space filled and
        right aligned """
    output = []
    base = len(chars)
    while x:
        rem = x % base
        output.append(chars[rem])
        x /= base
    if len(output) == 0:
        output.append(chars[0])
    final = ''.join(reversed(output))
    if len(final) > width:
        raise Exception('identifier() failed, not enough width.')
    return ('%' + str(width) + 's') % final

class categories(object):
    def __init__(self,name_list):
        pass
    def refine_max_category(self):
        """ perform one iteration of refinement, will divide the category with
        maximum size """
        pass
    def refine_one_char(self):
        """ perform one iteration of refinement, will divide the category using
        one more unique chars, applied to category with the least number of unique
        chars """
        pass
    def max_category_size(self):
        pass
    def num_categories(self):
        """ current number of categories """
        pass
    def categories(self):
        pass

def auto_group_names(name_list, optimal):
    """ automatically group a list of names (of equal length) using wildcast,
    initial single group of 'match all' will be divided into groups by using
    more and more unique leading characters, until number of groups exceeds
    the specified optimal value. """
    def getGroups(name_list,categories):
        """ split names into groups each matching an entry in categories """
        import re
        grouping = {}
        for g in categories:
            m = re.compile(g)
            grouping[g] = [n for n in name_list if m.match(n) is not None]
        return grouping
    def maxGroup(groups):
        """ given a dictionary of category and its matching name list, finds
        the key of the group with maximum length """
        gs = groups.keys()
        ns = [len(groups[g]) for g in gs]
        return gs[ns.index(max(ns))]
    def refineGroups(name_list,groups):
        """ perform one iteration of group refinement, by spliting the largest
        category into subgroups, groups is a dict of categories and matched name
        list. """
        # find max len category
        max_cat = maxGroup(groups)
        max_list = groups[max_cat]
        name_len = len(name_list[0])
        new_wc_i = max_cat.find('.') + 1
        if new_wc_i in [0,name_len+1]: return None
        # refine max category
        gs = []
        for n in max_list:
            if n[:new_wc_i] not in gs:
                gs.append(n[:new_wc_i])
        new_categories = [g+'.'*(name_len-new_wc_i) for g in gs]
        new_groups = getGroups(max_list,new_categories)
        # update groups, NOTE this modifies the argument passed in
        groups.pop(max_cat)
        for nc in new_groups:
            groups[nc] = new_groups[nc]
        return groups
    def cleanUp(groups):
        """ check if any category having only one match, remove the wildcast char """
        for g in groups:
            ms = groups[g]
            if len(set(ms)) == 1:
                groups.pop(g)
                groups[ms[0]] = ms
        return groups

    # init groups
    categories = ['.'*len(name_list[0])]
    groups = getGroups(name_list,categories)
    while len(groups) < optimal:
        new_groups = refineGroups(name_list,groups)
        if new_groups is None:
            return cleanUp(groups)
    return cleanUp(groups)

def column_track_polyline(geo, line):
    """ (multi-segment) line should be specified like this:
    [ np.array([2775133.9, 6282885.4]),
      np.array([2780517.2, 6280577.0]),
      np.array([2780617.2, 6280077.0]) ] """
    def robust_column_track(geo, line_segment):
        """ this does the same as mulgrid's column_track() method, but with a more
        robust (but potentially expensive) wrapper """
        # this is to overcome the unpredictable cases of column_track() failure
        # usually it can be solved by slightly nudge the line around
        tr = None
        nudge = 1.0e-8
        n_try = 0
        while (tr is None) and n_try < 10:
            n_try += 1
            try:
                tr = geo.column_track(line_segment)
                # print 'try', n_try
            except:
                tr = None
                # alternating nudge x or y to avoid 45deg case
                line_segment[1][n_try % 2] += nudge
                nudge *= 10.0
        return tr

    track = []
    for i in range(len(line)-1):
        t = robust_column_track(geo, line[i:i+2])
        if t is not None:
            track += t
        else:
            logging.error(''.join([
                'Failed to calculate column track (for vertical slices), ',
                'this line is too special. ',
                'It will be cool if you could send this to us to analyse.']))
            from pprint import pformat
            logging.error(pformat(line))
            return track
    return track

from contextlib import contextmanager
@contextmanager
def stdout_redirector(stream):
    import sys
    old_stdout = sys.stdout
    sys.stdout = stream
    try:
        yield
    finally:
        sys.stdout = old_stdout

if __name__ == '__main__':
    import numpy as np
    p1,p2=np.array([2.,1.]),np.array([3.,3.])
    p3 = np.array([2.,2.])
    c = (p1+p2)/2.0
    n1 = np.array([p1[1]-p2[1],p2[0]-p1[0]])
    n2 = -1. * n1
    print normal(p1,p2)
    print normal(p2,p1)
    print np.dot(p3-p1,normal(p1,p2))
