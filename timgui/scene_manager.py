"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt4.QtCore import *
from PyQt4.QtGui import *

from graphics_scenes import TopSurfaceScene, LayerScene, SliceScene
import numpy as np
from t2model import Model
from misc_funcs import column_track_polyline
import logging

class SceneManager(QObject):
    """ SceneManager is a class that manages all the scenes.  It also acts as
    the interface between scenes and outside world (a 'Facade' design pattern).
    There are a lot of necessary communications between a scene and many
    external objects/contorls.  Most of the the communications should be using
    SIGNALs, and the connection should probably be done and maintained in the
    SceneManager level.  So the outside world does not need to know how many
    scenes are there etc, or worry about as scenes are added and removed. """
    def __init__(self):
        super(SceneManager, self).__init__()
        self.scenelist = []
        self.scene = {}

        # keeps ref to those essential service when connected
        self._model = None
        self._color_bars = None
        self._arrow_scales = None

        # scene defs used to load/save user defined slices, layer not included
        #self._scenedefs = {}

        # this part probably needs re-write to be more flexible
        # ? how do I keep the dependency as well (future, eg. generatorations)
        self.color_vars = []
        self.label_vars = []
        self.arrow_vars = []

        # settings to be maintained, even if new scene is added, or scene item
        # setup delayed to display time.
        # call .updateSceneSettings()
        self.block_text_size = 1.0
        self.well_text_size = 1.0
        self.well_setting = {}
        self.well_setting_default = {
            'Track': {'color': 'black', 'visible': True},
            'Text': {'color': 'black', 'visible': True},
            'Head': {'color': 'black', 'visible': True},
        }
        self.well_label_pos = True #True corresponds to head, False to tail

        # a list of settings needs to be applied to new scenes when created
        # implemented as partials in dictionary, note currently these are only
        # good for those settings that was 'on/off' or single status, but not
        # those with scalling up/down things
        self._sync_new_scene = {}

        # signals to connect as new scenes created and disconnected as scenes
        # are destroyed
        self._signals_from_scene = []
        self._signals_to_scene = []

    def addSceneLayer(self,lay_i=None):
        """ create and add a layer scene, a SIGNAL will be emitted """
        geo = self._model.geo()
        if geo is None: raise Exception
        if lay_i is None:
            scene = TopSurfaceScene(self._model)
            scene.setupGraphicsPostponed(lay_i)
            scene.name = self._newName('Top Surface')
            self.addScene(scene)
        else:
            scene = LayerScene(self._model)
            scene.setupGraphicsPostponed(lay_i)
            layer = geo.layerlist[lay_i]
            scene.name = self._newName('Layer: %s %.1f' % (layer.name,layer.centre))
            self.addScene(scene)
        return scene.name

    def addSceneSlice(self,column_track,name='Slice',hide_wells_outside=500.0):
        """ create and add a slice scene from a PyTOUGH column_track,
        a SIGNAL will be emitted.  column_track is:
        1. a list of tuples (column,entrypoint,exitpoint)  OR
        2. a list of points """
        if self._model.geo() is None: raise Exception
        geo = self._model.geo()

        # TODO: change this to more robust, check if not column track object instead!!!
        # a column track is a list of tuples of (column,entrypoint,exitpoint)
        is_column_track = False
        try:
            float(column_track[0][0])
        except:
            is_column_track = True

        # if column_track is polyline, needs conversion to column_track
        # if type(column_track[0]) is type(np.array([])):
        if not is_column_track:
            line = column_track
            column_track = column_track_polyline(geo, line)
        if len(column_track) == 0:
            logging.error('Failed to create slice, column track length zero.')
            logging.info('Please redraw slice line if your line lies outside grid area.')
            return None

        # add scene
        scene = SliceScene(self._model)
        scene.setupGraphicsPostponed(column_track,hide_wells_outside)
        scene.name = self._newName(name)
        self.addScene(scene)
        return scene.name

    def addScene(self,scene):
        """ scene added must have an attribute .name, a signal will be emitted """

        # sync new scene with current settings
        for p in self._sync_new_scene.itervalues():
            p(scene)

        self.scenelist.append(scene)
        self.scene[scene.name] = scene

        for (signal_name,func) in self._signals_from_scene:
            self.connect(scene,SIGNAL(signal_name),func)
        for (obj,signal_name,scene_method_name) in self._signals_to_scene:
            self.connect(obj,SIGNAL(signal_name),getattr(scene,scene_method_name))

        self.emitSceneList()
        self.emit(SIGNAL("new_scene_added"),scene)

    def removeScene(self,name, emit_scene_list=True):
        if type(name) is type(1):
            i = name
            name = self.scenelist[i].name
        else:
            i = self.scenelist.index(self.scene[name])
        scene = self.scenelist.pop(i)

        for (signal_name,func) in self._signals_from_scene:
            self.disconnect(scene,SIGNAL(signal_name),func)
        for (obj,signal_name,scene_method_name) in self._signals_to_scene:
            self.disconnect(obj,SIGNAL(signal_name),getattr(scene,scene_method_name))

        del self.scene[name]
        # avoid unnecessary list update change (eg when remove all scenes)
        # needed if postponed setupGraphics()
        if emit_scene_list:
            self.emitSceneList()

    def removeAllScenes(self):
        for s in self.scene.keys(): self.removeScene(s,False)
        self.emitSceneList()

    def getNameList(self):
        return [s.name for s in self.scenelist]

    def _newName(self,base):
        base = str(base).strip()
        if base.strip() == '': base = 'Slice'
        import re
        pat = re.compile('\([0-9]+\)')
        while base in self.scene.keys():
            numtext = pat.findall(base)
            if len(numtext) <> 0:
                newnumtext = str(int(numtext[-1][1:-1])+1)
                base = re.sub(numtext[-1],newnumtext,base)
            else:
                base = base + '(1)'
        return base

    def _reloadGeo(self):
        """ clears all current scenes, re-load from model """
        import time
        geo = self._model.geo()
        self.removeAllScenes()
        self.addSceneLayer(None)
        start_time_0 = time.time()
        for i,lay in enumerate(geo.layerlist):
            start_time = time.time()
            self.addSceneLayer(i)
            msg = 'Finished creating layer %i after %.1f sec' % (i, (time.time() - start_time))
            logging.debug(msg)
        msg = 'Finished creating All after %.1f sec' % (time.time() - start_time_0)
        logging.debug(msg)

        track = column_track_polyline(geo, geo.bounds)
        # skip slice creation if really can't get column_track working
        if track:
            self.addSceneSlice(track)
        else:
            logging.error('Failed to create default slice (geo.bounds).')

        #clear well settings
        self.well_setting = {}

        # load default geo related variables
        if 'No Fill' not in self.color_vars:
            self.color_vars.insert(0,'No Fill')
            self.label_vars.insert(0,'No Text')
            self.arrow_vars.insert(0,'No Arrow')

        self.emitSceneList()
        self.emitVariableLists()

    def showSceneColor(self,val_name):
        if val_name:
            for s in self.scenelist:
                if s.views(): s.updateColor(val_name,self._color_bars[str(val_name)])

    def showSceneText(self,val_name):
        if val_name:
            for s in self.scenelist:
                if s.views(): s.updateText(val_name)

    def showSceneArrow(self,val_name):
        if val_name:
            for s in self.scenelist:
                if s.views(): s.updateArrow(val_name,self._arrow_scales[str(val_name)])

    def _updateArrowScale(self,val_name):
        """ arrow scale should be consistent with the manager stored internally
        as self._arrow_scales """
        if val_name:
            for s in self.scenelist:
                s.clearArrowCache(val_name)
                # TODO: check why this is needed
                if s.views(): s.updateArrow(val_name,self._arrow_scales[str(val_name)])

    def _updateScenesColor(self,val_name):
        """ Update all scenes' color of variable val_name by clear scenes' color
        cache. """
        if val_name:
            for s in self.scenelist:
                s.clearColorCache(val_name)

    def updateSceneSettings(self, scene=None):
        """ refreshes scene's text size, well settings etc to be sync with
        scene_manager """
        if scene is None:
            for s in self.scenelist:
                s.syncWellSetting(self.well_setting, self.well_setting_default)
                s.setBlockTextSize(self.block_text_size)
                s.setWellTextSize(self.well_text_size)
                s.toggleWellLabelPosition(self.well_label_pos)
        else:
            scene.syncWellSetting(self.well_setting, self.well_setting_default)
            scene.setBlockTextSize(self.block_text_size)
            scene.setWellTextSize(self.well_text_size)
            scene.toggleWellLabelPosition(self.well_label_pos)

    def updateWellSettings(self, newSettings):
        self.well_setting = newSettings

        for s in self.scenelist:
            if s.views(): #only refresh current scene
                s.syncWellSetting(self.well_setting, self.well_setting_default)

    def setWellLabelPos(self,pos):
        """pos can be either a boolean value (True = head, False = tail) or a string 'head' or 'tail' """
        if type(pos) == str:
            if pos.lower() == 'head':
                pos = True
            elif pos.lower() == 'tail':
                pos = False
            else:
                logging.error("Expected well position of bool or either 'head' or 'tail'")
                return

        self.well_label_pos = pos

        for s in self.scenelist:
            if s.views():
                s.toggleWellLabelPosition(pos)


    def blockTextLarger(self):
        self.block_text_size = self.block_text_size * 1.25
        for s in self.scenelist:
            if s.views():
                s.setBlockTextSize(self.block_text_size)

    def blockTextSmaller(self):
        self.block_text_size = self.block_text_size * 0.8
        for s in self.scenelist:
            if s.views():
                s.setBlockTextSize(self.block_text_size)

    def wellTextLarger(self):
        self.well_text_size = self.well_text_size * 1.25
        for s in self.scenelist:
            if s.views():
                s.setWellTextSize(self.well_text_size)

    def wellTextSmaller(self):
        self.well_text_size = self.well_text_size * 0.8
        for s in self.scenelist:
            if s.views():
                s.setWellTextSize(self.well_text_size)

    def getColorTextUnit(self, valName):
        """ returns the unit used by the scenes """
        # TODO: don't like so much checking for actual unit used by "scenes"
        u = None
        if valName in ['', 'No Fill', 'No Text', 'No Arrow']:
            return u
        for s in self.scenelist:
            u = s.getBlockUnit(valName)
            if u is not None:
                return u
        # only here if scenes have not cached the variable, or it really is None
        if self._model:
            u = self._model.getDataUnit(Model.BLOCK, valName)
        # if still None, it really is None
        return u

    def setColorTextUnit(self, valName, newunit):
        for s in self.scenelist:
            s.changeBlockUnit(valName, newunit)

    def getFlowUnit(self, valName):
        """ returns the unit used by the scenes """
        # TODO: don't like so much checking for actual unit used by "scenes"
        u = None
        if valName in ['', 'No Fill', 'No Text', 'No Arrow']:
            return u
        for s in self.scenelist:
            u = s.getFlowUnit(valName)
            if u is not None:
                return u
        # only here if scenes have not cached the variable, or it really is None
        if self._model:
            u = self._model.getDataUnit(Model.FLOW, valName)
        # if still None, it really is None
        return u

    def setFlowUnit(self, valName, newunit):
        for s in self.scenelist:
            s.changeFlowUnit(valName, newunit)

    def _rawDataChanged(self,(mdtype,valName,change)):
        """
        called when model's raw data changes:
            1. clear indivisual scene's cache
            2. change list of variables accordingly
         mdtype: Model.BLOCK or Model.FLOW
         change: 'added', 'updated', or 'removed'
        """
        #print "Model's raw %s data '%s' is %s" % (mdtype,valName,change)
        if mdtype is Model.BLOCK:
            if change is Model.ADDED:
                u = self._model.getDataUnit(mdtype, valName)
                logging.debug("Model data '%s' has unit '%s'" % (valName, u))
                self.setColorTextUnit(valName, u)
                self.color_vars.append(valName)
                self.label_vars.append(valName)
                self.emitVariableLists()
            elif change is Model.UPDATED:
                for s in self.scenelist:
                    s.clearBlockCache(valName)
            elif change is Model.REMOVED:
                self.color_vars.remove(valName)
                self.label_vars.remove(valName)
                self.emitVariableLists()
                for s in self.scenelist:
                    s.clearBlockCache(valName)
        elif mdtype is Model.FLOW:
            if change is Model.ADDED:
                self.setFlowUnit(valName, self._model.getDataUnit(mdtype, valName))
                self.arrow_vars.append(valName)
                self.emitVariableLists()
            elif change is Model.UPDATED:
                for s in self.scenelist:
                    s.clearFlowCache(valName)
            elif change is Model.REMOVED:
                self.arrow_vars.remove(valName)
                self.emitVariableLists()
                for s in self.scenelist:
                    s.clearFlowCache(valName)

    def emitSceneList(self):
        """ notify related components to update scene list """
        self.emit(SIGNAL("scene_list_updated"), self.getNameList())

    def emitVariableLists(self):
        """ notify related components to update variable list """
        # don't send var list when there is no scene at all
        if len(self.scenelist) == 0: return

        #sort variable lists before emitting them
        self.color_vars = self.color_vars[0:1] + sorted(self.color_vars[1:])
        self.label_vars = self.label_vars[0:1] + sorted(self.label_vars[1:])
        self.arrow_vars = self.arrow_vars[0:1] + sorted(self.arrow_vars[1:])

        self.emit(SIGNAL("color_vars_updated"), self.color_vars)
        self.emit(SIGNAL("label_vars_updated"), self.label_vars)
        self.emit(SIGNAL("arrow_vars_updated"), self.arrow_vars)

    def connectSignalFromScene(self,signal_name,func):
        """ Register connection of scene's signal to given callable.
        SceneManager will ensure all scenes connect properly.  This takes into
        account of scenes being added and removed over time, and all the
        existing scenes.  Note func here should be an bound method (a method
        attached to an instance.)."""
        self._signals_from_scene.append((signal_name,func))
        for s in self.scenelist:
            QObject.connect(s, SIGNAL(signal_name), func)

    def connectSignalToScene(self,obj,signal_name,scene_method_name):
        """ Register connection from external obj's signal to scene's method.
        SceneManager will ensure all scenes connect properly.  This takes into
        account of scenes being added and removed over time, and all the
        existing scenes. """
        self._signals_to_scene.append((obj,signal_name,scene_method_name))
        for s in self.scenelist:
            QObject.connect(obj,SIGNAL(signal_name),getattr(s,scene_method_name))

    def connectModel(self,model):
        """ allow scene react to loading/reloading of model geo/dat/lst etc """
        if self._model is not None:
            # disconnect all signals from the previous model, (needs to be
            # checked to see if working properly at all
            self.disconnect(self._model,SIGNAL("model_geo_reloaded"),self._reloadGeo)
            self.disconnect(self._model,SIGNAL("model_raw_data_changed"),self._rawDataChanged)

        self._model = model
        self.connect(self._model,SIGNAL("model_geo_reloaded"),self._reloadGeo)
        self.connect(self._model,SIGNAL("model_raw_data_changed"),self._rawDataChanged)

    # These are connections from external controls to each/all scenes
    def connectInfoBoard(self,info_board):
        self.connectSignalFromScene("scene_message",info_board.showMessage)

    def connectSelectionBoard(self,selection_board):
        self.connectSignalFromScene("scene_multi_selected",selection_board.multiSelect)
        self.connectSignalFromScene("scene_single_selected",selection_board.singleSelect)
        self.connectSignalToScene(selection_board,"set_single_select","setSingleSelect")
        self.connectSignalToScene(selection_board,"set_multi_select_update","setMultiSelectUpdate")
        self.connectSignalToScene(selection_board,"set_multi_select_complete","setMultiSelectComplete")

    def connectPointsBoard(self,points_board):
        self.connectSignalFromScene("scene_polyline_updated",points_board.loadPolyline)
        self.connectSignalToScene(points_board,"send_polyline","loadPolyline")
        self.connectSignalToScene(points_board,"clear_polyline","clearPolyline")

    def connectGenerTable(self,gener_table):
        self.connectSignalFromScene("scene_single_selected",gener_table.singleSelect)

    def connectModeManager(self,mode_manager):
        """ this connects to a mode manager so that all scenes are connected and
        reflecting the mode from mode manager. """
        self.connectSignalFromScene("scene_mode_changed", mode_manager.setMode)
        self.connectSignalToScene(mode_manager, "mode_changed", "setMode")

        for s in self.scenelist:
            s.setMode(mode_manager.currentMode())

        # for use when new scene created
        from functools import partial
        def set_scene_mode(scene,mode_manager):
            scene.setMode(mode_manager.currentMode())
        self._sync_new_scene['setMode'] = partial(set_scene_mode,
            mode_manager=mode_manager)

    # needs more work
    def connectColorBars(self,color_bars):
        """ Colorbar manager informs when a cb object updates, which triggers
        all scenes to clear color of the particular variable.

        This connects self (scene mnager) to colorbar manager.  Actually it also
        keeps a internal direct reference to the colorbar manager, similar to
        ._model and ._arrow_scales

        TODO: need more work, why keep cb manager as internal object?
        """
        if self._color_bars:
            self.disconnect(self._color_bars,SIGNAL("color_bar_updated"),self._updateScenesColor)
        self._color_bars = color_bars
        if self._color_bars:
            self.connect(self._color_bars,SIGNAL("color_bar_updated"),self._updateScenesColor)

    def connectArrowScaleManager(self,arrow_scale_manager):
        if self._arrow_scales:
            self.disconnect(self._arrow_scales,SIGNAL("arrow_scale_updated"),self._updateArrowScale)
        self._arrow_scales = arrow_scale_manager
        if self._arrow_scales:
            self.connect(self._arrow_scales,SIGNAL("arrow_scale_updated"),self._updateArrowScale)

