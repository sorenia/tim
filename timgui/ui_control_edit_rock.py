"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt4.QtCore import *
from PyQt4.QtGui import *

from graphics_scenes import *
from ui_control_mode import Ui_SceneModeControl
from t2model import Model
from selection import BlockRockEntry, Selection
import misc_funcs # access funcs by name
import waiwera_input
import settings
import logging

class ApplyRockButton(QWidget):
    """ allow users to select between different ways of 'applying rock' """
    def __init__(self, parent=None):
        super(ApplyRockButton, self).__init__(parent)
        self._validator = QRegExpValidator(QRegExp('[+*]{5}'))

        self.option_group = QGroupBox('Advanced Option')
        self.option_group.setLayout(QGridLayout())
        self.stacked_apply_button = QStackedLayout()

        self.setLayout(QVBoxLayout())
        self.layout().addWidget(self.option_group)
        self.layout().addLayout(self.stacked_apply_button)

        self._addOptions({
            'r_title': 'Copy Rock Property:',
            'e_default': '*****',
            'e_tip': "5 chars of '+' and '*': + to keep target name, * to replace",
            'b_title': '&Apply Rocktype',
            'b_tip': "Copy Source Rock Property to Target Blocks",
            'func_name': "update_block_rock_property",
            })

        self._addOptions({
            'r_title': 'Copy Rock Name:',
            'e_default': '**+++',
            'e_tip': "5 chars of '+' and '*': + to keep target name, * to replace",
            'b_title': '&Apply Rockname',
            'b_tip': "Copy Source Rock Name to Target Blocks",
            'func_name': "update_block_rock_name",
            })

        # toggle to the default option (toggle ensures everything init nicely)
        self.option_group.layout().itemAtPosition(0,0).widget().toggle()

        self.loadSettings()

    def _addOptions(self, option):
        from functools import partial

        r = QRadioButton(option['r_title'])
        e = QLineEdit(option['e_default'])
        e.current_value = option['e_default']
        e.setToolTip(option['e_tip'])
        # editingFinished() signals will only be emitted if the validator returns QValidator::Acceptable
        # e.setValidator(self._validator)
        b = QPushButton(QIcon(":/down.png"),option['b_title'])
        b.setToolTip(option['b_tip'])
        self.stacked_apply_button.addWidget(b)

        # use the count to know which position to insert, each row keeps 2
        i = self.option_group.layout().count() / 2
        self.option_group.layout().addWidget(r, i, 0, Qt.AlignLeft)
        self.option_group.layout().addWidget(e, i, 1, Qt.AlignLeft)

        QObject.connect(r, SIGNAL('toggled (bool)'), e.setEnabled)
        QObject.connect(r, SIGNAL('clicked ()'),
            partial(self.stacked_apply_button.setCurrentWidget, b))
        QObject.connect(e, SIGNAL("editingFinished ()"),
            partial(self._validateConvention, e))
        QObject.connect(b, SIGNAL("clicked ()"),
            partial(self._emitApply, option['func_name'], e))
        # NOTE: partial() freezes some arguments, so cannot pass e.current_value

        r.toggle()

    def _validateConvention(self, line_edit):
        """ this is called everytime user finished editing convention, if user
        entry is incorrect, it will be reversed to last good value.  Also
        line_edit.current_value will always be up-to-date. """
        v = self._validator
        conv = line_edit.text()
        conv_len = conv.length()
        state, conv_len = v.validate(conv, conv_len)
        if state <> QValidator.Acceptable:
            line_edit.setText(line_edit.current_value)
        else:
            line_edit.current_value = str(line_edit.text())

    def _emitApply(self, func_name, e):
        logging.debug("SIGNAL: apply_rock, %s, %s" % (func_name, str(e.text())))
        self.emit(SIGNAL("apply_rock"), func_name, str(e.text()))

    def triggerApply(self):
        self.stacked_apply_button.currentWidget().click()

    def loadSettings(self):
        # hide option group if not in advanced mode
        s = settings.userSettings()
        advanced = s.value("advanced_rocktype_editor", False).toBool()
        self.toggleAdvanced(advanced)

    def toggleAdvanced(self, adv=None):
        """ if adv specified, will use is as bool value, otherwise toggle
        current state"""
        if adv is None:
            self.option_group.setVisible(not self.option_group.isVisible())
        else:
            self.option_group.setVisible(adv)

class Ui_EditRocktypeBoard(QWidget):
    """  High level UI that allows editing model's rocktype assignments etc.
    Also contains the core UI that deals with multi selection. """
    def __init__(self, parent=None):
        super(Ui_EditRocktypeBoard, self).__init__(parent)
        self._model = None
        self.pick = BlockRockEntry(None,'')
        self.selection = Selection()

        # ------------------ Special selection mode control ----------
        self.mode_control = Ui_SceneModeControl()
        QObject.connect(self.mode_control, SIGNAL("set_scene_mode"),self._notify)
        mbs = self.mode_control.button()
        single_button = mbs[MyScene.SINGLE_SELECTION_MODE]
        single_button.setToolTip("Pick from Block in Scenes")
        multi_button  = mbs[MyScene.MULTI_SELECTION_MODE]
        multi_button.setToolTip("Pick Blocks from Scenes")

        #------------------- Source UI widgets -----------------------
        self.block_label = QLabel()
        self.single_edit = QLineEdit()
        single_label = QLabel("Using Rock:")
        single_label.setBuddy(self.single_edit)

        self.show_button = QToolButton()
        self.show_button.setIcon(QIcon(":/rocks_table.png"))
        self.show_button.setToolTip("Pick from ROCKS Table")
        QObject.connect(self.show_button,SIGNAL("clicked ()"),self.showRocktypeTable)

        #------------------- Apply UI widgets -----------------------
        self.apply_rock = ApplyRockButton()
        QObject.connect(self.apply_rock, SIGNAL('apply_rock'), self._applyRock)

        #------------------- Target UI widgets -----------------------
        self.table = QTableView()
        self.table.setModel(self.selection)
        multi_label = QLabel("List of target blocks:")
        multi_label.setBuddy(self.table)

        self.clear_button = QPushButton('Clear Selection')
        QObject.connect(self.clear_button,SIGNAL("clicked ()"),self.clearMultiSelect)
        self.copy_button = QPushButton('&Copy Selection')
        QObject.connect(self.copy_button,SIGNAL("clicked ()"),self.copySelected)

        #------------------- Layouts ------------------------
        groupbox_source = QGroupBox("Source Rocktype")
        layout_source = QBoxLayout(QBoxLayout.LeftToRight)
        layout_source.addWidget(single_label)
        layout_source.addWidget(self.block_label)
        layout_source.addWidget(self.single_edit)
        layout_source.addWidget(single_button)
        layout_source.addWidget(self.show_button)
        groupbox_source.setLayout(layout_source)

        layout_apply = QBoxLayout(QBoxLayout.TopToBottom)
        layout_apply.addWidget(self.apply_rock)

        groupbox_target = QGroupBox("Target Blocks")
        layout_target = QBoxLayout(QBoxLayout.TopToBottom)
        layout_t1 = QBoxLayout(QBoxLayout.LeftToRight)
        layout_t1.addWidget(multi_label)
        layout_t1.addStretch(1)
        layout_t1.addWidget(multi_button)
        layout_t3 = QBoxLayout(QBoxLayout.LeftToRight)
        layout_t3.addWidget(self.copy_button)
        layout_t3.addWidget(self.clear_button)
        layout_target.addLayout(layout_t1)
        layout_target.addWidget(self.table)
        layout_target.addLayout(layout_t3)
        groupbox_target.setLayout(layout_target)

        layout = QBoxLayout(QBoxLayout.TopToBottom)
        layout.addWidget(groupbox_source)
        layout.addLayout(layout_apply)
        layout.addWidget(groupbox_target)
        self.setLayout(layout)

    def multiSelect(self,name,objType,select):
        """ call this when selection made one by one """
        # at the moment assume al objType is SceneData.BLOCK_ITEM
        if select:
            self.selection.add(name)
        else:
            self.selection.remove(name)
        self.emit(SIGNAL("set_multi_select_update"),name,objType,select)

    def singleSelect(self,name,objType):
        self.pick.entry = name
        rock = ''
        if self.pick.rocks:
            rock = self.pick.rocks[0]

        self.block_label.setText('('+name+')')
        # waiwera uses rock index, au/tough2 uses string rock name
        self.single_edit.setText(str(rock))
        self.emit(SIGNAL("set_single_select"),name,objType)

    def setRockPick(self,name):
        self.block_label.setText('')
        # waiwera uses rock index, au/tough2 uses string rock name
        self.single_edit.setText(str(name))

    def showRocktypeTable(self):
        self.emit(SIGNAL("show_rock_table"))

    def _applyRock(self, func_name, convention):
        """ need function name (in module misc_funcs) and convention, designed
        to be connected to ApplyRockButton objects, which is a group of wigets
        that controlls settings such as func_name and convention. """
        if self._model is None: return
        if self._model.dat() is None: return
        use_rock = str(self.single_edit.text())
        blist = list(self.selection.selected())
        logging.debug(' '.join([a.__repr__() for a in [func_name, use_rock, 'on:', blist]]))

        if self._model.dat_simulator in ['waiwera']:
            #!! -1 means no rock assigned if apply this
            #!! important to offset the index back to waiwera
            geo = self._model.geo()
            cell_idx = [geo.block_name_index[b]-geo.num_atmosphere_blocks for b in blist]
            waiwera_input.update_cells_rock(self._model.dat(), cell_idx, int(use_rock))
        elif self._model.dat_simulator in ['tough2', 'autough2']:
            getattr(misc_funcs, func_name)(self._model.dat(), blist, use_rock, convention)
        self._model.setDatEdited(refresh_tables=True)  # make sure model knows it's been modified

        self.clearMultiSelect()

    def applyRocktype(self):
        """ for external calls such as command, hence able to be in menu """
        self.apply_rock.triggerApply()

    def copySelected(self):
        """ copy to clipboard a string of selected blocks names in python style """
        txt = "['" + "','".join(self.selection.selected()) + "']"
        clipboard = QApplication.clipboard()
        clipboard.setText(txt)

    def clearSingleSelect(self):
        self.block_label.setText('')
        self.single_edit.setText('')

    def clearMultiSelect(self,objType=SceneData.BLOCK_ITEM):
        self.selection.clear()
        self.emit(SIGNAL("set_multi_select_complete"), self.selection.selected(), objType)

    def _notify(self,mode):
        """ to notify scene what's selected when switching between multi
        and single selection mode """
        if mode == MyScene.MULTI_SELECTION_MODE:
            self.emit(SIGNAL("set_multi_select_complete"), self.selection.selected(), SceneData.BLOCK_ITEM)
        elif mode == MyScene.SINGLE_SELECTION_MODE:
            self.emit(SIGNAL("set_single_select"), str(self.pick.entry), SceneData.BLOCK_ITEM)

    def connectModel(self,model):
        if self._model:
            # disconnect
            QObject.disconnect(self._model,SIGNAL("model_dat_reloaded"),self.clearMultiSelect)
            QObject.disconnect(self._model,SIGNAL("model_dat_reloaded"),self.clearSingleSelect)
        self._model = model
        if self._model:
            # connect
            QObject.connect(self._model,SIGNAL("model_dat_reloaded"),self.clearMultiSelect)
            QObject.connect(self._model,SIGNAL("model_dat_reloaded"),self.clearSingleSelect)

        # do these anyway
        self.pick = BlockRockEntry(model,self.pick.entry)
        self.selection.connectModel(model)

    def connectRockTable(self, rock_table):
        self.connect(rock_table,SIGNAL("rocktype_selected"),self.setRockPick)

if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)

    test = Ui_EditRocktypeBoard()
    test.show()
    test.selection.add('xxx')
    test.selection.remove('xyz12a')
    test.selection.clear()

    app.exec_()



