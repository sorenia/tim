"""
Functional testing of TIM, supports both with and without GUI event loop.
"""

"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

import unittest
import threading
import time

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.QtTest import QTest

import sys
app = QApplication(sys.argv)
app.setOrganizationName("University of Auckland")
app.setOrganizationDomain("auckland.ac.nz")
app.setApplicationName("Tim isn't Mulgraph")
app.setWindowIcon(QIcon(":/rabbit_large.png"))

class TimFunctionalTest(unittest.TestCase):
    """ Experiment with functional testing """
    def setUp(self):
        self._exec_used = False
        from timgui.ui_main_window import MainWindow
        self.tim = MainWindow()
        self._loadTestingSettings()
        self.tim.show()

    def tearDown(self):
        if not self._exec_used:
            # just to ensure the gui is closed
            self._restoreUserSettings()
            self.tim.close()
        del self.tim

    def _loadTestingSettings(self):
        """ keep users' settings aside, load settings for testing """
        # TODO: other settings as well
        self._original_path = self.tim._current_path
        self.tim._current_path = '.'

    def _restoreUserSettings(self):
        """ restore user's settings """
        self.tim._current_path = self._original_path

    def exec_(self, test_list, delay=None):
        """ Call this to start the event loop, so GUI can react after each of
        the testing expression.  The test_list is a list of functions/lambdas,
        each will be executed in term after GUI processed all queued events. The
        event loop will end when the main window is no longer visible, this is
        triggered by main window's .close() method, which will be appended to
        the list automatically.  If delay is given, the test will be paused for
        delay seconds in between each test. See:
        http://www.sidefx.com/docs/houdini11.0/hom/cookbook/pyqt/part1/ """
        self._exec_used = True

        # !!! original mechanism of running Qt's GUI app:
        # global app
        # app.exec_()

        # !!! this can replace the original app.exec_():
        # loop = QEventLoop()
        # need to have MainWindow().closeEvent() to emit SIGNAL('quit')
        # QObject.connect(self.tim, SIGNAL('quit'), loop.exit)
        # loop.exec_()

        # NOTE: it's better to let the test class to deal with closing main
        # window, so that it has chance to restore the settings
        test_list += [
            lambda: self._restoreUserSettings(),
            lambda: self.tim.close(),
            ]

        if delay is not None:
            import time
            test_list_new = [lambda: time.sleep(delay)] * (2 * len(test_list) - 1)
            test_list_new[::2] = test_list
        else:
            test_list_new = test_list

        global app
        loop = QEventLoop()
        while self.tim.isVisible():
            loop.processEvents()
            app.sendPostedEvents(None, 0)
            if test_list_new:
                test_list_new.pop(0)()

    def ex_test_exp1(self):
        """ example of not using event loop ie. not calling exec() """

        self.tim.setWindowTitle('XXXXXX'),
        self.tim.run_command('about'),
        self.assertTrue(self.tim.about.isVisible(), msg='about dialog should show'),
        self.tim.about.accept(),
        self.assertFalse(self.tim.about.isVisible(), msg='about dialog should hide'),
        self.tim.setWindowTitle('YYYYYY'),
        self.assertEqual(self.tim.windowTitle(), 'YYYYYY', msg='set title y'),
        self.tim.run_command('open_geo', '../examples/GWAI1515_AW_05.DAT'),
        self.assertTrue(self.tim.model.geo() is not None, msg='geo loaded'),

    def ex_test_exp2(self):
        """ example with event loop, build a list of tests, then exec() """
        # why is about not blocking, even with dialog set to modal

        # self.tim.run_command('open_geo')
        # On Windows the dialog will spin a blocking modal event loop that will
        # not dispatch any QTimers, and if parent is not 0 then it will position
        # the dialog just below the parent's title bar.
        # QTimer.singleShot(5000, self.tim.close)
        # print '++++2 close:', self.tim.close()

        do_list = [
            lambda: self.tim.setWindowTitle('XXXXXX'),
            lambda: self.tim.run_command('about'),
            lambda: self.assertTrue(self.tim.about.isVisible(), msg='about dialog should show'),
            lambda: self.tim.about.accept(),
            lambda: self.assertFalse(self.tim.about.isVisible(), msg='about dialog should hide'),
            lambda: self.tim.setWindowTitle('YYYYYY'),
            lambda: self.assertEqual(self.tim.windowTitle(), 'YYYYYY', msg='set title y'),
            lambda: self.tim.run_command('open_geo', '../examples/GWAI1515_AW_05.DAT'),
            lambda: self.assertTrue(self.tim.model.geo() is not None, msg='geo loaded'),
            ]

        # this replaces the original GUI event loop, now always execute a test
        # after finishing all events
        self.exec_(do_list, 0.5)

    def test_load_(self):
        """
        Apple tries TIM:
        - Apple launches TIM
        - Apple opens geo GWAI1515_AW_05.DAT
        + Apple sees top surface in view, all columns visible and in place
        + Apple sees num layers + 3 scenes (atm, top, slice) loaded in scene list
        - Apple clicks on scene AT
        - Apple sees layers AT: 1 column visible (a column polygan and an empty text)
        - Apple clicks on scene SB
        - Apple sees layers SB: 3 columns visible
        + Apple chooses variable color 'Surf'
        + Apple sees same layer, but filled in colors
        + Apple turns off vairable color
        + Apple sees same layer, but no color.
        - Apple closes TIM
        """
        self.tim.run_command('open_geo', '../examples/GWAI1515_AW_05.DAT'),
        self.assertTrue(self.tim.model.geo() is not None, msg='geo loaded'),

        # change to AT
        lws = self.tim.scene_control.LayerSliceSelector.findItems('AT', Qt.MatchContains | Qt.MatchCaseSensitive)
        self.assertEqual(len(lws), 1)
        self.tim.scene_control.LayerSliceSelector.setCurrentItem(lws[0])
        self.assertEqual(set(self.getSceneItemNames(['Block'], True)), set(['AT  0']))

        # change to SB
        lws = self.tim.scene_control.LayerSliceSelector.findItems('SB', Qt.MatchContains | Qt.MatchCaseSensitive)
        self.assertEqual(len(lws), 1)
        self.tim.scene_control.LayerSliceSelector.setCurrentItem(lws[0])
        self.assertEqual(set(self.getSceneItemNames(['Block'], True)), set(['SB666','SB268','SB367']))

        # change variable color to surf
        i = self.tim.scene_control.ColorSelector.findText('Surf', Qt.MatchContains | Qt.MatchCaseSensitive)
        self.tim.scene_control.ColorSelector.setCurrentIndex(i)

    def getSceneItemNames(self, obj_types=[], visible=True):
        """ Return a list of name of QGraphicsItem in the mian scene.  Note only
        TIM's special QGraphicsItems will go through, which support .objType and
        other related properties. """
        its = [it for it in self.tim.main_view.scene().items() if hasattr(it, 'objType')]
        return [it.name for it in its if it.isVisible() is visible and it.objType in obj_types]

if __name__ == '__main__':
    unittest.main(verbosity=2)

