### File formats

Because TIM uses many modules of PyTOUGH directly, many of the supported file formats are identical to PyTOUGH's.

**Geometry file**: (uses PyTOUGH's `mulgrids` module)  Currently TIM uses Mulgraph geometry format.  Note that only the `GENERal` type in Mulgraph geometry format is supported at the moment.  For details please see Appendix A in the ['*PyTOUGH user's guide*'](https://github.com/acroucher/PyTOUGH/blob/master/doc/PyTOUGH- guide.pdf?raw=true).  Support for the `RADIAl` and `RECTAngular` types and other geometry formats (such as GMESH) may be supported in the future.

**Input file**: (uses PyTOUGH's `t2data` module)  Again, most variants of TOUGH2 simulator family's input files are supported.

**Listing file**: (uses PyTOUGH's `t2listing` module)  This supports most outputs from TOUGH2, TOUGH2-MP, AUTOUGH2, TOUGH+, TOUGH-REACT.  Note only the data in the form of *listing tables* are supported, and these tables has to be full tables (eg. element table for all blocks).

There are also some miscellaneous file formats frequently used by TIM. 

**JSON file**: ["JavaScript Object Notation"](http://json.org/) is a data-exchange format designed to be easily imported/exported by programs, as well as easy to be inspected/edited by human.  For example, TIM can import/export a list of line plots in a JSON file.  You can: prepare/edit the file manually, write code to generate it, or use external code to render them with a different graph engine. 

An example of TIM's plots in .json file:

```
[
    {
        "series": [
            {
                "variable": "Temperature", 
                "type": "HistoryBlockSeries", 
                "block": "SD 32"
            }, 
            {
                "transposexy": false, 
                "type": "HistoryFieldDataSeries", 
                "filename": "C:/temp/pressure.dat"
            }
        ], 
        "ylabel": "Time (s)", 
        "xlabel": "Temperature (C)", 
        "title": "plot1"
    }, 
    {
        "series": [
            {
                "variable": "Generation rate", 
                "gener": "PRD12", 
                "type": "HistoryGenerSeries"
            }, 
            {
                "transposexy": true, 
                "type": "HistoryFieldDataSeries", 
                "filename": "C:/temp/massflow"
            }
        ], 
        "ylabel": "Time (s)", 
        "xlabel": "Massflow (kg/s)", 
        "title": "plot2", 
        "properties": {
            "xlimits": [0.0, 1000.0 ],
            "ylimits": [0.0, 50.0 ]
        }
    }
]
```

For a list of plots, TIM expects the whole json file as a list (`[]`) of plot objects (`{}`).  Each plot object has several `"key": value` pairs.  The `"series"` key should be a list of series objects.  A series object again has several pairs of keys and values.  Note the comma used to separate different objects.

TIM expects these keys and values to follow certain rules, but the rules are designed to work with a wide range of possibilities.  For example, you can find three possible different `"type"` of series objects here: `"HistoryGenerSeries"`, `"HistoryFieldDataSeries"`, or `"HistoryBlockSeries"`.  For each type you will need to provide some properties, such as `"filename"` or `"variable"`.  I will write out a proper reference for these rules.

Note that the second plot in the example contains an additional `"properties"` which is a dictionary that is available for other additional/non-essential properties to be kept with the list.  TIM will keep them intact in the import/export cycle.  You can be creative about things to store here.

### License

TIM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TIM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TIM.  If not, see <http://www.gnu.org/licenses/>.

The source code can be found on the Bitbucket website at <https://bitbucket.org/angusyeh/tim>.

