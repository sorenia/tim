
TIM allows you to interact with the underlying Python objects at run-time through the
Interactive Console.

---

### Running commands

Many of the actions you perform under GUI are actually implemented as commands.
These commands can also be triggered from the interactive console.  This can be
useful when you want to perform some actions repeatedly.

In some cases, certain features can only be triggered via the console commands.
A couple of experimental features are hidden this way.  Users are also able to
create a command and drop in as a plugin.  These are generally executed using
the console too.

    >>> list_commands()
    ...
    >>> run_commands('a_command_name')

> *Tip: The command structure also serves as the main mechanism of assigning
[Shortcut Keys](howto.md#customisable-shortcuts) to various tasks.*

### Interact with PyTOUGH objects

You can directly interact with PyTOUGH objects directly through the console.
The three main objects *mulgrid* (geometry file), *t2data* (input file),
*t2listing* (output data) are exposed by

    >>> model.geo()
    >>> model.dat()
    >>> model.lst()

### Interact with TIM's objects

Taking advantage of Python's dynamic nature, interactive console allows you to
access TIM's underlying mechanism directly, in sync with the GUI.  Often this is
used  to debug or develop new features.  But it is also very useful if you are
interested in developing new commands.  Knowledge of PyQt is helpful if you are
interested in operating at this level.

Some of the useful components that you may want to interact with:

* `window` (TIM's main application window)
* `colorbar_manager`
* `points_board`

### Tips

It very common to print variables/objects in Interactive Console.  *pprint* is a
Python standard library that prints many Python structures nicely on screen.

    >>> from pprint import pprint as pp
    >>> pp(model.dat().generator)

I think I will fix this, but at the moment, PyQt modules is not imported to be seen inside interactive console.  Hence you may need to import them before use.

    from PyQt4.QtCore import *
    from PyQt4.QtGui import *

---

The following are tips to perform specific tasks in the interactive console.

---

### Edit colorbar programmically

    cb = colorbar_manager['Temperature']
    cb.valueScale = [0., 50., 100., 150., 200., 250., 300.,350.]
    cb.colorScale = [QColor(c) for c in ['blue','skyblue','lightgreen','yellow','orange','orangered','red']]
    cb.discreteColorMode = True
    colorbar_manager['Temperature'] = cb

Note how the particular colorbar object is obtained first, then edited, and lastly re-assigned back to `colorbar_manager`.  The last assignment is to trigger the refreshing mechanism within TIM (otherwise colorbar manager does not even know its object has been edited).

### Hide or show some of the wells

    scene_manager.setWellVisible(False)
    scene_manager.setWellVisible(True, include=['Track', 'Head'])
    scene_manager.setWellVisible(True, wells=['TH...'], include=['Text'])

The first line hides all wells.  Then well tracks and well heads (dots) of all wells are then turned visible (but not the text/names).  The last line only turns on the well name labels on for wells names that starts with 'TH'.

